################################
#       TIME SERIES PLOT       #
################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

#         UNIQUE IOCs          #
file = 'openioc-ipv4'

csv = []
with open(file) as f:
    for line in f:
        csv.append(line.strip('./') + '\t' + f.next())
days, iocs = np.loadtxt(csv, unpack=True, converters={ 0: mdates.strpdate2num('%Y-%m-%d')})

plt.plot_date(x=days, y=iocs)
plt.show()

