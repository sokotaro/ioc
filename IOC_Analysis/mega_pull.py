
###########################################################
##                                                       ##
##          IOC MEGA PULL                                ##
##                                                       ##
##                                                       ##
##                                                       ##
###########################################################
import os
import sys
import netaddr
import requests
import MySQLdb as mdb
from datetime import datetime, timedelta

def classify(*args):
    info = ' '.join(args).replace(',',' ').replace('.',' ').replace('#',' ').replace('/',' ').replace('*',' ').replace('-',' ').replace('_',' ').replace('\"',' ')
    info = info.lower().split()
    info = [x.strip(' ') for x in info]
    info = set(info)
    ransomware = set(['ransomwaretracker','ranscam', 'ransom','ransomware','.cryptohasyou.', '777', '7ev3n', '7h9r', '8lock8', 'alfa', 'alpha', 'amba', 'apocalypse', 'angler', 'apocalypsevm', 'autolocky', 'badblock', 'bandarchor', 'bart', 'bitcryptor', 'bitstak', 'blackshades', 'crypter', 'blocatto', 'booyah', 'brazilian', 'brlock', 'browlock', 'bucbi', 'buyunlockcode', 'cerber', 'chimera', 'coinvault', 'coverton', 'cryaki', 'crybola', 'cryfile', 'crypren', 'crypt', 'crypt38', 'cryptear', 'cryptfile2', 'cryptinfinite', 'cryptobit', 'cryptodefense', 'cryptofinancial', 'cryptofortress', 'cryptographic', 'locker', 'cryptohost', 'cryptojoker', 'cryptolocker', 'cryptomix', 'cryptoroger', 'cryptoshocker', 'cryptotorlocker2015', 'cryptowall', 'cryptowall', 'cryptowall', 'cryptowall', 'cryptxxx', 'cryptxxx', 'cryptxxx', 'ctb-faker', 'ctb-locker', 'ctb-locker', 'web', 'cuteransomware', 'decrypt', 'protect', 'dedcryptor', 'dirtydecrypt', 'dmalocker', 'dmalocker', 'eda2', 'hiddentear', 'educrypt', 'el-polocker', 'enigma', 'fakben', 'fonco', 'fury', 'ghostcrypt', 'gnl', 'locker', 'gomasom', 'goopic', 'gopher', 'hawkeye', 'harasom', 'herbst', 'hi', 'buddy!', 'hydracrypt', 'ilock', 'ilocklight', 'international', 'police', 'association', 'jeiphoos', 'jigsaw', 'job', 'crypter', 'keranger', 'keybtc', 'keyholder', 'kimcilware', 'kozy.jozy', 'kratoscrypt', 'kryptolocker', 'lechiffre', 'linux.encoder', 'locker', 'locky', 'lortok', 'lowlevel04', 'mabouia', 'magic', 'maktublocker', 'mircop', 'mireware', 'mischa', 'mm', 'locker', 'mobef', 'nanolocker', 'nemucod', 'odcodc', 'offline', 'omg!', 'operation', 'global', 'iii', 'padcrypt', 'pclock', 'petya', 'pizzacrypts', 'powerware', 'powerworm', 'prism', 'raa', 'encryptor', 'radamant', 'rakhni', 'rannoh', 'ransom32', 'rector', 'remindme', 'rokku', 'samas-samsam', 'sanction', 'satana', 'scraper', 'shujin', '[6~skidlocker', 'pompous', 'snslocker', 'sport', 'strictor', 'surprise', 'synolocker', 'szflocker', 'teslacrypt', 'teslacrypt', 'threat', 'finder', 'torrentlocker', 'towerweb', 'toxcrypt', 'troldesh', 'truecrypter', 'umbrecrypt', 'ungluk', 'unlock92', 'vaultcrypt', 'virlock', 'virus-encoder', 'wildfire', 'locker', 'xorist', 'xrtn', 'zcrypt', 'zimbra', 'zlader', 'russian', 'zyklon','ghost','kovter','ransomwarecerber'])

    malware = set(['ursnif','palevo','zeus','threatcrowd','dga','exploitskit','exploit','kitten','autoshun','atrack','pua','dynamoo','kit','virus','malware','worm','trojan','trojans','bot','bots','botnet','botsnet','botnets','spyware','rootkit','root','kit','torjan','cnc','c2c','c2','c&c','exploit','cerber','locky','lizard','backdoor','tsunami','stdbot','kaiten','antichrist','infection','nuclear', 'command', 'control', 'malc0de','pony','steam','stealer','marcos','dns','unlocker','otx','kaixin','tofsee','scareware','iheate','dridex','hisec','espionage','malwr','fakeav','keybase','adwind','asprox','keylogger','tiny','banker','bbsrat','keybase','compile','sploit','powersploit','power','post','key','logger','ddos','linux','windows','microsoft','android','apple','mac','win32','win','unix','apt','shellshock','qbot','ipbl','macro','exe','dropper','pdf','attachment','attachments','doc','png','cymon','rat','rats','osx','os','netwire','macro','vba','w2k','w2km','threat','advanced','persistent','blockbuster','lazarus','kraken','jrat','metel','infection','elf','excel','zip','word','black','energy','exploitkit','volexity','abuse','banload','game','blackenergy','blackenergy3'])

    phishing = set(['phish','probing','openphish','spear','spearphishing','badfish','phis','phising','scam','fish','badfish','superfish','fish','fraud','mroutlet','hostpatching','mimic','phishing'])

    spam = set(['snort','iprep','bogon','response','nullsecure','uceprotect','scam','backscatter','scamming','scammer', 'spam', 'spammer', 'spamming'])

    bruteforce = set(['badips','attackers','bruteforceblocker','dragonresearchgroup','greensnow','nullsecure','binarydefense','openbl','blocklist','honey','honeypot','brute','bruteforce','force','kippo','scan','scans','honeypots','probe','sshpsychos','ssh','psychos','scanning','scanner','intrusion','army','flood','flooders','attacks','ddos','tunnel','tunneling','ssh'])

    hijacked = set(['cins','compromised','attack','breached','suspicious','emergingthreats','spamhaus','hijacked','hi-jacked','taken','hacked','tetra29','malacious','host','github'])

    proxy = set(['open','relay','anonymous','vpn','proxy','proxylists','tor','torlist','exit','torrent','tor_exit_node','proxychanger'])

    if info.intersection(ransomware):
        return 'ransomware'
    elif info.intersection(spam):
        return 'spam'
    elif info.intersection(phishing):
        return 'phishing'
    elif info.intersection(bruteforce):
        return 'bruteforce'
    elif info.intersection(hijacked):
        return 'hijacked'
    elif info.intersection(malware):
        return 'malware'
    elif info.intersection(proxy):
        return 'proxy'
    else:
        return 'unknown'


def scrape(feed):
    try:
        feed()
    except Exception as inst:
        with open('error.log', 'a') as f:
            f.write('\nError processing: ' + str(feed) + ' : ' + datetime.today().date().isoformat() + '\n')
            f.write(str(inst) + '\n')
#            f.write(str(sys.exc_traceback.tb_lineno) + '\n')
            f.write('Error on line: ' + str(sys.exc_info()) + '\n')

def run(query):
    with con:
        cursor = con.cursor()
        cursor.execute(query)
    return cursor


def saveattack(*args):
    if len(args) == 4:
        attack = args[0]
        ioc_feed = args[1]
        ioc_date = args[2]
        ioc_data = args[3]
        with open('attackip/' + attack + '/' + ioc_feed + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')        
    else:
        attack = args[0]
        ioc_date = args[1]
        ioc_data = args[2]
        with open('attackip/' + attack + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')

def saveall(*args):
    if len(args) == 4:
        ioc_type = args[0]
        ioc_feed = args[1]
        ioc_date = args[2]
        ioc_data = args[3]
        with open('all/' + ioc_type + '/' + ioc_feed + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')        
    else:
        ioc_type = args[0]
        ioc_date = args[1]
        ioc_data = args[2]
        with open('all/' + ioc_type + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')

def savetip(*args):
    if len(args) == 4:
        ioc_type = args[0]
        ioc_feed = args[1]
        ioc_date = args[2]
        ioc_data = args[3]
        with open('tip/' + ioc_type + '/' + ioc_feed + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')        
    else:
        ioc_type = args[0]
        ioc_date = args[1]
        ioc_data = args[2]
        with open('tip/' + ioc_type + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')

def save(*args):
    if len(args) == 4:
        ioc_type = args[0]
        ioc_feed = args[1]
        ioc_date = args[2]
        ioc_data = args[3]
        with open('openioc/' + ioc_type + '/' + ioc_feed + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')        
    else:
        ioc_type = args[0]
        ioc_date = args[1]
        ioc_data = args[2]
        with open('openioc/' + ioc_type + '/' + ioc_date, 'a') as f:
            f.write(ioc_data + '\n')

def mkattack(attack, ioc_feed):
    if not os.path.exists('attackip/' + attack + '/' + ioc_feed):
        os.makedirs('attackip/' + attack + '/' + ioc_feed)

def mkall(ioc_type, ioc_feed):
    if not os.path.exists('all/' + ioc_type + '/' + ioc_feed):
        os.makedirs('all/' + ioc_type + '/' + ioc_feed)

def mktip(ioc_type, ioc_feed):
    if not os.path.exists('tip/' + ioc_type + '/' + ioc_feed):
        os.makedirs('tip/' + ioc_type + '/' + ioc_feed)

def mkdir(ioc_type, ioc_feed):
    if not os.path.exists('openioc/' + ioc_type + '/' + ioc_feed):
        os.makedirs('openioc/' + ioc_type + '/' + ioc_feed)

def checkall(ioc_type, ioc_date):
    return os.path.isfile('all/' + ioc_type + '/' + ioc_date)

def checktip(ioc_type, ioc_date):
    return os.path.isfile('tip/' + ioc_type + '/' + ioc_date)

def check(ioc_type, ioc_date):
    return os.path.isfile('openioc/' + ioc_type + '/' + ioc_date)


def alien():
    print 'processing alien-vault-otx feeds...'

    ioc_feed = 'alien'
    query    = 'SELECT indicator, type, created, name, tags FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_type = line[1]
        ioc_date = line[2].split('T')[0]

        if ioc_type == 'IPv4':
            ioc_type = 'ipv4'
            attack = classify(line[3].strip(), line[4].strip())
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)

        elif ioc_type == 'IPv6':
            ioc_type = 'ipv6'

        elif ioc_type == 'CIDR':
            ioc_type = 'cidr'

        elif ioc_type == 'CVE':
            ioc_type = 'cve'

        elif ioc_type == 'domain':
            ioc_type = 'domain'

        elif ioc_type == 'email':
            ioc_type = 'email'

        elif ioc_type == 'FileHash-IMPHASH':
            ioc_type = 'imphash'

        elif ioc_type == 'FileHash-MD5':
            ioc_type = 'md5'

        elif ioc_type == 'FileHash-PEHASH':
            ioc_type = 'pehash'

        elif ioc_type == 'FileHash-SHA1':
            ioc_type = 'sha1'

        elif ioc_type == 'FileHash-SHA256':
            ioc_type = 'sha256'

        elif ioc_type == 'FilePath':
            ioc_type = 'filepath'

        elif ioc_type == 'hostname':
            ioc_type = 'hostname'

        elif ioc_type == 'Mutex':
            ioc_type = 'mutex'

        elif ioc_type == 'URI':
            ioc_type = 'uri'

        elif ioc_type == 'URL':
            ioc_type = 'url'

        else:
            with open('error.log', 'a') as f:
                f.write('ERROR: | ' + ioc_feed + ' | ' + ioc_type + ' | ' + ioc_data + ' | ' + ioc_date + '\n') 

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def anti():
    print 'processing anti-hacker-alliance feeds...'

    ioc_feed = 'anti'
    ioc_type = 'ipv4'
    query    = 'SELECT ip, reported, date_time FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    mkdir(ioc_type, ioc_feed)
    mkall(ioc_type, ioc_feed)
    attack = 'bruteforce'
    mkattack(attack, ioc_feed)
    
    for line in data:
        ioc_data = line[0]
        reported = line[1]
        ioc_date = line[2].split(' ')[0]
                    
#        if not check(ioc_type, ioc_date):
#        for x in xrange(int(reported)):
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)
        '''
        else:
            tmp = set(line.strip() for line in open('data/' + ioc_type + '/' + ioc_date))
            if not tmp.intersection(ioc_data):
                for x in xrange(int(reported)):
                    save(ioc_type, ioc_date, ioc_data)
                    save(ioc_type, ioc_feed, ioc_date, ioc_data)
        '''
        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def bambenek():
    print 'processing bambenek-consulting feeds...'

    ioc_feed = 'bambenek'
    ioc_type = 'ipv4'
    query    = 'SELECT ip, reported, server FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    mkdir(ioc_type, ioc_feed)
    mkall(ioc_type, ioc_feed)

    for line in data:
        ioc_data = line[0]
        ioc_date = line[1].split(' ')[0]
        attack = classify(line[2].strip())
        mkattack(attack, ioc_feed)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def blocklist():
    print 'processing blocklist-de feeds...'

    ioc_feed = 'blocklist'

    if 'All' in table:
        query    = 'SELECT ip FROM ' + table
        data     = run(query)
        total    = data.rowcount
        count    = 0

        for line in data:
            ioc_data = line[0]
            ioc_date = '-'.join(table.split('_')[4:])

            if ':' in ioc_data:
                ioc_type = 'ipv6'
            else:
                ioc_type = 'ipv4'
                attack = 'bruteforce'
                mkattack(attack, ioc_feed)
                saveattack(attack, ioc_date, ioc_data)
                saveattack(attack, ioc_feed, ioc_date, ioc_data)


            mkall(ioc_type, ioc_feed)
            mkdir(ioc_type, ioc_feed)            
            save(ioc_type, ioc_date, ioc_data)
            save(ioc_type, ioc_feed, ioc_date, ioc_data)
            saveall(ioc_type, ioc_date, ioc_data)
            saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

            count += 1
            if (count % (total/10)) == 0:
                print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'  
    else:
        query    = 'SELECT ip, last_attack FROM ' + table
        data     = run(query)
        total    = data.rowcount
        count    = 0

        for line in data:
            ioc_data = line[0]
            ioc_date = line[1].split(' ')[0]

            if not ioc_date:
                ioc_date = '-'.join(table.split('_')[3:])

            if ':' in ioc_data:
                ioc_type = 'ipv6'
            else:
                ioc_type = 'ipv4'
                attack = 'bruteforce'
                mkattack(attack, ioc_feed)
                saveattack(attack, ioc_date, ioc_data)
                saveattack(attack, ioc_feed, ioc_date, ioc_data)

            mkall(ioc_type, ioc_feed)
            mkdir(ioc_type, ioc_feed)            
            save(ioc_type, ioc_date, ioc_data)
            save(ioc_type, ioc_feed, ioc_date, ioc_data)
            saveall(ioc_type, ioc_date, ioc_data)
            saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

            count += 1
            if (count % (total/10)) == 0:
                print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def charles():
    print 'processing charles-haley feeds...'

    ioc_feed = 'charles'
    ioc_type = 'ipv4'
    query    = 'SELECT ip, date_reported FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    mkall(ioc_type, ioc_feed)
    mkdir(ioc_type, ioc_feed)

    for line in data:
        ioc_data = line[0]
        ioc_date = line[1].strip('()')
        ioc_date = '-'.join((ioc_date.split('/')[:2] + ['20'+ioc_date.split('/')[2]])[::-1])

        attack = 'bruteforce'
        mkattack(attack, ioc_feed)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)


        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def critical():
    print 'processing critical-stack feeds...'

    ioc_feed = 'critical'
    query    = 'SELECT fields_indicator, indicator_type, meta_source FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    for line in data:

        ioc_data = line[0].strip()
        ioc_type = line[1].strip('Intel::')
        ioc_date = '-'.join(table.split('_')[2:])

        if ioc_type == 'ADDR':
            if ':' not in ioc_data:
                ioc_type = 'ipv4'
                attack = classify(line[2].strip())
                mkattack(attack, ioc_feed)
                saveattack(attack, ioc_date, ioc_data)
                saveattack(attack, ioc_feed, ioc_date, ioc_data)

            else:
                ioc_type = 'ipv6'

        elif ioc_type == 'DOMAIN':
            ioc_type = 'domain'

        elif ioc_type == 'FILE_HASH':
            if len(ioc_data) == 32:
                ioc_type = 'md5'
            elif len(ioc_data) == 40:
                ioc_type = 'sha1'
            elif len(ioc_data) == 64:
                ioc_type = 'sha256'
            else: 
                ioc_type = 'hash'
                with open('error.log', 'a') as f:
                    f.write('ERROR: | ' + ioc_feed + ' | ' + ioc_type + ' | ' + ioc_data + ' | ' + ioc_date + '\n') 

        elif ioc_type == 'URL':
            ioc_type = 'url'

        elif ioc_type == 'EMAIL':
            ioc_type = 'email'

        elif ioc_type == 'FILE_NAME':
            ioc_type = 'filename'

        else:
            with open('error.log', 'a') as f:
                f.write('ERROR: | ' + ioc_feed + ' | ' + ioc_type + ' | ' + ioc_data + ' | ' + ioc_date + '\n') 

        mkall(ioc_type, ioc_feed)
        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def banned():
    print 'processing banned-hacker-ips feed...'

    ioc_feed = 'banned'
    ioc_type = 'ipv4'
    query    = 'SELECT ip, date_reported FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    attack   = 'bruteforce'
    mkattack(attack, ioc_feed)
    mkall(ioc_type, ioc_feed)
    mkdir(ioc_type, ioc_feed)

    for line in data:
        ioc_data = line[0]
        ioc_date = line[1]

        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def dshield():
    print 'processing dshield-attack feeds...'

    ioc_feed = 'dshield'
    ioc_type = 'ipv4'
    query    = 'SELECT ip reported FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0
    attack   = 'bruteforce'
    mkattack(attack, ioc_feed)
    mkall(ioc_type, ioc_feed)
    mkdir(ioc_type, ioc_feed)

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[1:])
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

def feodo():
    print 'processing feodo-attack feeds...'

    ioc_feed = 'feodo'
    query    = 'SELECT ioc, ioc_type, attack FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[1:])
        ioc_type = line[1]

        if ioc_type == 'ipv4':
            attack = classify(line[2].strip())
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)


        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

def palevo():
    print 'processing palevo-attack feeds...'

    ioc_feed = 'palevo'
    query    = 'SELECT ioc, ioc_type, attack FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[1:])
        ioc_type = line[1].lower()
        
        if ioc_type == 'ipv4':
            attack = classify(line[2].strip())
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)

def edrop():
    print 'processing drop-cidr blocks...'

    ioc_feed = 'edrop'
    query    = 'SELECT ioc, ioc_type FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[2:])
        ioc_type = line[1]

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

def drop():
    print 'processing drop-cidr blocks...'

    ioc_feed = 'drop'
    query    = 'SELECT ioc, ioc_type FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[2:])
        ioc_type = line[1]

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

def zeus():
    print 'processing zeus-cidr blocks...'

    ioc_feed = 'zeus'
    query    = 'SELECT ioc, ioc_type, attack FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0]
        ioc_date = '-'.join(table.split('_')[1:])
        ioc_type = line[1]

        attack = classify(line[2].strip())
        mkattack(attack, ioc_feed)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

def csirt():
    print 'processing csirt feeds......'

    ioc_feed = 'csirt'
    query    = 'SELECT ioc_data, ioc_type FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0].lower()
        ioc_date = '-'.join(table.split('_')[3:])
        ioc_type = line[1].lower()

        if 'email' in ioc_type or 'e-mail' in ioc_type:
            ioc_type = 'email'

        elif 'cidr' in ioc_type:
            ioc_type = 'cidr'

        elif 'filename' in ioc_type or 'winfile' in ioc_type:
            ioc_type = 'filename'

        elif 'md5' in ioc_type:
            ioc_type = 'md5'

        elif 'regkey' in ioc_type:
            ioc_type = 'regkey'

        elif 'string' in ioc_type:
            ioc_type = 'string'

        elif 'url' in ioc_type:
            ioc_type = 'url'

        elif 'domain' in ioc_type or 'c2' in ioc_type:
            ioc_type = 'domain'

        elif 'uri' in ioc_type:
            ioc_type = 'uri'

        elif 'useragent' in ioc_type:
            ioc_type = 'useragent'

        elif 'sha1' in ioc_type:
            ioc_type = 'sha1'

        elif 'sha256' in ioc_type:
            ioc_type = 'sha256'

        elif 'ssdeep' in ioc_type:
            ioc_type = 'ssdeep'

        elif 'ipv4' in ioc_type:
            ioc_type = 'ipv4'
            attack = 'malware'
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)

        else:
            ioc_type = 'error'
            with open('error.log', 'a') as f:
                f.write('ERROR: | ' + ioc_feed + ' | ' + ioc_type + ' | ' + ioc_data + ' | ' + ioc_date + '\n')
        if ioc_type != 'error':
            mkall(ioc_type, ioc_feed)
            saveall(ioc_type, ioc_date, ioc_data)
            saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

            mktip(ioc_type, ioc_feed)
            savetip(ioc_type, ioc_date, ioc_data)
            savetip(ioc_type, ioc_feed, ioc_date, ioc_data)  


def sourcefire():
    print 'processing sourcefire feeds......'

    ioc_feed = 'sourcefire'
    query    = 'SELECT ioc_data, ioc_type, attack FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0].lower()
        ioc_date = '-'.join(table.split('_')[1:])
        ioc_type = line[1].lower()
        
        if ioc_type == 'ipv4':
            attack = classify(line[2].strip())
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mktip(ioc_type, ioc_feed)
        savetip(ioc_type, ioc_date, ioc_data)
        savetip(ioc_type, ioc_feed, ioc_date, ioc_data)  

def ransomware():
    print 'processing ransomware feeds......'

    ioc_feed = 'ransomware'
    query    = 'SELECT ip FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        if line != '':
            ioc_data = line[0].lower()
            ioc_date = '-'.join(table.split('_')[1:])
            ioc_type = 'ipv4'

            attack = 'ransomware'
            mkattack(attack, ioc_feed)
            saveattack(attack, ioc_date, ioc_data)
            saveattack(attack, ioc_feed, ioc_date, ioc_data)

            mkall(ioc_type, ioc_feed)
            saveall(ioc_type, ioc_date, ioc_data)
            saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

            mkdir(ioc_type, ioc_feed)
            save(ioc_type, ioc_date, ioc_data)
            save(ioc_type, ioc_feed, ioc_date, ioc_data)

def talos():
    print 'processing talos feeds......'

    ioc_feed = 'talos'
    query    = 'SELECT ip FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0].lower()
        ioc_date = '-'.join(table.split('_')[2:])
        ioc_type = 'ipv4'

        attack = 'hijacked'
        mkattack(attack, ioc_feed)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

def longtail():
    ioc_feed = 'longtail'
    query    = 'SELECT ip, date FROM ' + table
    data     = run(query)
    total    = data.rowcount
    count    = 0

    for line in data:
        ioc_data = line[0].lower()
        ioc_date = line[1][:10].replace('/','-').replace('.','-')
        ioc_type = 'ipv4'

        attack = 'bruteforce'
        mkattack(attack, ioc_feed)
        saveattack(attack, ioc_date, ioc_data)
        saveattack(attack, ioc_feed, ioc_date, ioc_data)

        mkall(ioc_type, ioc_feed)
        saveall(ioc_type, ioc_date, ioc_data)
        saveall(ioc_type, ioc_feed, ioc_date, ioc_data)

        mkdir(ioc_type, ioc_feed)
        save(ioc_type, ioc_date, ioc_data)
        save(ioc_type, ioc_feed, ioc_date, ioc_data)

print 'Connecting to openioc database...'

con    = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
query  = 'show tables'
tables = run(query)
tb_set = [table[0] for table in tables]  #  tb_set = [table[0] for table in tables][::-1]

if __name__ == "__main__":
    cmd = "find openioc/ -type f -name '*' -delete"
    print cmd
    os.system(cmd)
    cmd = "find all/ -type f -name '*' -delete"
    print cmd
    os.system(cmd)
    cmd = "find tip/ -type f -name '*' -delete"
    print cmd
    os.system(cmd)
    cmd = "find attackip/ -type f -name '*' -delete"
    print cmd
    os.system(cmd)

    for table in tb_set:
#        if table == 'Alien_Vault_OTX_Python':
        if table =='Alien_Vault_OTX_Python_2016_12_20':
            scrape(alien)
        if table == 'Anti_Hacker_Alliance_Black_List_365': 
            scrape(anti)
        if 'Bambenek_Consulting' in table:
            print table
            scrape(bambenek)
        if 'Blocklist_DE_IP' in table:
            print table
            scrape(blocklist)
        if table == 'Charles_Haley_SSH':
            scrape(charles)
        if 'Critical_Stack_2' in table:
            print table
            scrape(critical)
        if table == 'Banned_Hacker_IPs':
            scrape(banned)
        if 'DShield' in table:
            scrape(dshield)
        if 'RansomWare_' in table:
            scrape(ransomware)
        if table == 'Long_Tail':
            scrape(longtail)
        if 'Talos_' in table:
            scrape(talos)

        # This is with TIP 
        if ('Feodo' in table) and ('OLD' not in table):
            scrape(feodo)
        if ('Palevo' in table) and ('OLD' not in table):
            scrape(palevo)
        if ('Spamhaus_Drop_' in table) and ('OLD' not in table):
            scrape(drop)
        if ('Spamhaus_EDROP_' in table) and ('OLD' not in table):
            scrape(edrop)
        if ('ZeuS' in table) and ('OLD' not in table):
            scrape(zeus)
        if 'CSIRT' in table:
            scrape(csirt)
        if 'Sourcefire' in table:
            scrape(sourcefire)
