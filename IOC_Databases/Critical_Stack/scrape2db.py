#!/usr/bin/python
# -*- coding: utf-8 -*-
from OTXv2 import OTXv2
from pandas.io.json import json_normalize
from datetime import datetime, timedelta
import MySQLdb as mdb
import sys

con = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')

file = 'master-public.bro.dat'

fileData = []

#with con:
#    cur = con.cursor()
    #-----------------------------------------------------#
    #--------------------CRITICAL STACK-------------------#
    #-----------------------------------------------------#
#    cur.execute("DROP TABLE IF EXISTS Critical_Stack")
#    cur.execute("CREATE TABLE Critical_Stack(Id INT PRIMARY KEY AUTO_INCREMENT, \
#                 fields_indicator VARCHAR(256), indicator_type VARCHAR(32), meta_source VARCHAR(256), do_notice VARCHAR(128))")

#    f = open(file)
#    i = 0

#    print('Uploading Critical-Stack Data')
#    for line in f:
#        data = line.split('\t')
#        fileData.append(data)
#        cur.execute("INSERT INTO Critical_Stack(fields_indicator, indicator_type, meta_source, do_notice) VALUES(%s,%s,%s,%s)",(fileData[i][0],fileData[i][1],fileData[i][2],fileData[i][3],))
#        i += 1
#        if (i % 1000) == 0:
#            print('*'),

#    cur.execute("SELECT * FROM Critical_Stack")

#    rows = cur.fetchall()

#    for row in rows:
#        print row
    #-----------------------------------------------------#
    #--------------------CRITICAL STACK-------------------#
    #-----------------------------------------------------#

    #-----------------------------------------------------#
    #--------------------OTX ALIEN VAULT------------------#
    #-----------------------------------------------------#

otx = OTXv2("0df1996cb4a5ccfeab82bffac382b244f04f192f249cff1753d2e9e0caab684a")
pulses = otx.getall()
#print(len(pulses))
#print(pulses)

#print(json_normalize(pulses[0]))
for line in pulses:
    print(json_normalize(line["indicators"]))

    #-----------------------------------------------------#
    #--------------------OTX ALIEN VAULT------------------#
    #-----------------------------------------------------#
