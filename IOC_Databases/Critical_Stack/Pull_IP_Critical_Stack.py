import sys
'''
sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')
'''

import locale
import MySQLdb as mdb
import time
import os

output = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    print 'Querying for ip addresses'

    cursor.execute("SELECT fields_indicator FROM Critical_Stack WHERE indicator_type = 'Intel::ADDR'")	
    for ip in cursor:
        if ":" not in str(ip[0]):
            output.append(str(ip[0])+'\n')

    cursor.execute("SELECT fields_indicator FROM Critical_Stack_01_04_2016 WHERE indicator_type = 'Intel::ADDR'")
    for ip in cursor:
        if ":" not in str(ip[0]):
            output.append(str(ip[0])+'\n')
    
    cursor.execute("SELECT fields_indicator FROM Critical_Stack_11_03_2015 WHERE indicator_type = 'Intel::ADDR'")	
    for ip in cursor:
        if ":" not in str(ip[0]):
            output.append(str(ip[0])+'\n')

    cursor.execute("SELECT fields_indicator FROM Critical_Stack_2016_02_07 WHERE indicator_type = 'Intel::ADDR'")	
    for ip in cursor:
        if ":" not in str(ip[0]):
            output.append(str(ip[0])+'\n')
        
    cursor.execute("SELECT fields_indicator FROM Critical_Stack_2016_04_10 WHERE indicator_type = 'Intel::ADDR'")	
    for ip in cursor:
        if ":" not in str(ip[0]):
            output.append(str(ip[0])+'\n')

        
print 'Writing to File: Critical_Stack_IP_Adressess'
with open('Critical_Stack_IP_Adressess.txt', 'w') as f:
    f.writelines(output)
	
cmd = 'cat Critical_Stack_IP_Adressess.txt | sort -u | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n > /home/katayama/Desktop/home/IOC_Databases/COMPARE_DATA/critical_stack.txt'
print cmd
os.system(cmd)
