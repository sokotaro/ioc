#!/usr/bin/python
import sys
#sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')

# -*- coding: utf-8 -*-
from OTXv2 import OTXv2
from pandas.io.json import json_normalize
from datetime import datetime, timedelta
import MySQLdb as mdb
import os
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

con = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')

file = 'master-public.bro.dat'

with con:
    cur = con.cursor()
    
    cur.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Critical_Stack'")
    create_time = cur.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info((create_time))
    
    cmd = "RENAME TABLE Critical_Stack TO Critical_Stack_" + yesterday
    try:
        cur.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass
    #-----------------------------------------------------#
    #--------------------CRITICAL STACK-------------------#
    #-----------------------------------------------------#
#    cur.execute("DROP TABLE IF EXISTS Critical_Stack")
    cur.execute("CREATE TABLE Critical_Stack(Id INT PRIMARY KEY AUTO_INCREMENT, fields_indicator VARCHAR(255), indicator_type VARCHAR(255), meta_source VARCHAR(255), do_notice VARCHAR(128))")
    
    f = open(file)
    count = 0
    cmd   = 'cat ' + file + ' | wc -l'
    total = int(os.popen(cmd).read().strip())

    logging.info(('Uploading Critical-Stack Data'))
    for line in f:
        data = line.strip().split('\t')
        if '#' not in data[0]:
            cur.execute("INSERT INTO Critical_Stack(fields_indicator, indicator_type, meta_source, do_notice) VALUES(%s,%s,%s,%s)",(data[0],data[1],data[2],data[3],))

        count += 1
        if (count % (total/10)) == 0:
            logging.info('Processed: ' + str((10*count)/(total/10)) + '% | ' + str('{:,}'.format(count)) + ' IOCs')

#    cur.execute("SELECT * FROM Critical_Stack")

#    rows = cur.fetchall()

#    for row in rows:
#        print row
    #-----------------------------------------------------#
    #--------------------CRITICAL STACK-------------------#
    #-----------------------------------------------------#

