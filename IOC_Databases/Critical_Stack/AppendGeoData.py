#!/usr/bin/python
# -*- coding: utf8 -*-
import sys
#sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')

import MySQLdb as mdb
import time
import os

con = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')
#latitude DECIMAL(10,8 )
#longitude DECIMAL(11, 8)
with con:
    cursor = con.cursor()
    cursor.execute("DROP TABLE IF EXISTS Critical_Stack_Addresses_With_GEO")
    cursor.execute("CREATE TABLE Critical_Stack_Addresses_With_GEO(ip VARCHAR(16), country_code VARCHAR(8), \
                    country_name VARCHAR(128), region_code VARCHAR(16), region_name VARCHAR(128), city VARCHAR(128), \
                    zip_code VARCHAR(12), time_zone VARCHAR(128), latitude VARCHAR(16), longitude VARCHAR(16), \
                    metro_code VARCHAR(8))")
    query = ("SELECT fields_indicator FROM Critical_Stack_Addresses")
    cursor.execute(query)
    i = 0
    for (fields_indicator) in cursor:
        result = os.popen("curl localhost:8080/json/"+fields_indicator[0]).read()
        result = result.replace("{", "")
        result = result.replace("}", "")
        result = result.replace("\"", "")
        result = result.split(",")
        
        ip = country_code = country_name = region_code = region_name = city = zip_code = latitude = longitude = metro_code = " "

        ip = result[0].split(":")
        if(len(ip) > 1):
            ip = ip[1]
        else:
            ip = ip[0]
        print ip

        country_code = result[1].split(":")
        if(len(country_code) > 1):
            country_code = country_code[1]
        else:
            country_code = country_code[0]
#        print country_code

        country_name = result[2].split(":")
        if(len(country_name) > 1):
            country_name = country_name[1]
        else:
            country_name = country_name[0]

#        print region_code
        region_code = result[3].split(":")
        if(len(region_code) > 1):
            region_code = region_code[1]
        else:
            region_code = region_code[0]
#        print region_name

        region_name = result[4].split(":")
        if(len(region_name) > 1):
            region_name = region_name[1]
        else:
            region_name = region_name[0]
#        print city
        
        city = result[5].split(":")
        if(len(city) > 1):
            city = city[1]
        else:
            city = city[0]
#        print zip_code

        zip_code = result[6].split(":")
        if(len(zip_code) > 1):
            zip_code = zip_code[1]
        else:
            zip_code = zip_code[0]
#        print zip_code

        time_zone = result[7].split(":")
        if(len(time_zone) > 1):
            time_zone = time_zone[1]
        else:
            time_zone = time_zone[0]
#        print time_zone
        
        latitude = result[8].split(":")#float(result[8].split(":")[1])
        if(len(latitude) > 1):
            latitude = latitude[1]
        else:
            latitude = latitude[0]
#        print latitude

        longitude = result[9].split(":")
        if(len(longitude) > 1):
            longitude = longitude[1]
        else:
            longitude = longitude[0]
#        print longitude

        metro_code = result[10].split(":")
        if(len(metro_code) > 1):
            metro_code = metro_code[1]
        else:
            metro_code = metro_code[0]
#        print metro_code
        
        cursor.execute("INSERT INTO Critical_Stack_Addresses_With_GEO(ip, country_code, country_name, region_code, region_name, city, zip_code, time_zone, latitude, longitude, metro_code) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(ip,country_code,country_name,region_code,region_name,city,zip_code,time_zone,latitude,longitude,metro_code,))
        
        i += 1
        if (i % 10) == 0:
            print('*'),
            break
        
#        for (line) in result:
#            print(line)
#        time.sleep(1)
    cursor.close()
    con.close()
