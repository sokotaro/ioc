import MySQLdb as mdb
import datetime
import os
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
data = []
with open('monitor/monitor_'+yesterday) as f:
    for line in f:
        line = line.strip()
        if '#' not in line:
            data.append(line.split(','))

#  MySQL does not like '-'
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    query = "CREATE TABLE Bambenek_Consulting_" + yesterday + "(ip VARCHAR(16) NOT NULL, server VARCHAR(255), reported VARCHAR(255), source VARCHAR(255))"
    
    try:
        cursor.execute(query)
    except Exception as e:
        logging.debug(e)

    filename = 'ip-filter.blf'

    total= len(data)
    logging.info('num_lines = ' + str("{:,}".format(total)))

    count = 0
#    print yesterday
    for line in data:
        if len(line) > 3: 
            ip       = line[0]
            server   = line[1]
            reported = line[2]
            source   = line[3]
        
            query = "INSERT INTO Bambenek_Consulting_" + yesterday + "(ip, server, reported, source) VALUES(%s,%s,%s,%s)"
            cursor.execute(query,(ip, server, reported, source))

            count += 1
            if (count % (total/10)) == 0:
                logging.info('Processed: ' + str((10*count)/(total/10)) + '% | ' + str('{:,}'.format(count)) + ' ip-blocks')
