################################################################
##                                                            ##
##     https://zeustracker.abuse.ch/blocklist.php             ##
##                                                            ##
##     https://zeustracker.abuse.ch/monitor.php?filter=all    ##
##     ZeuS IP blocklist "BadIPs"                             ##
##                                                            ##
################################################################
import sys
from datetime import datetime, timedelta
import locale
import MySQLdb as mdb
import time
import os
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday.replace('-','_')+'.log',level=logging.DEBUG)

cmd = 'wget https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist -O data/zeus_baddomains_'+yesterday+'.txt'
logging.info((cmd))
os.system(cmd)
filename1 = 'data/zeus_baddomains_'+yesterday+'.txt'
    
cmd = 'wget https://zeustracker.abuse.ch/blocklist.php?download=ipblocklist -O data/zeus_badips_'+yesterday+'.txt'
logging.info((cmd))
os.system(cmd)
filename2 = 'data/zeus_badips_'+yesterday+'.txt'
    
cmd = 'wget https://zeustracker.abuse.ch/blocklist.php?download=compromised -O data/zeus_compromised_'+yesterday+'.txt'
logging.info((cmd))
os.system(cmd)
filename3 = 'data/zeus_compromised_'+yesterday+'.txt'


data = []
with open(filename1) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'domain' + '\t' + 'ZeuS C&C' + '\n')

with open(filename2) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'ipv4' + '\t' + 'ZeuS C&C' + '\n')

with open(filename3) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'url' + '\t' + 'ZeuS C&C' + '\n')

    
#  MySQL does not like '-'
yesterday = yesterday.replace('-','_')

logging.info('Connecting to openioc database...')
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
with con:
    cursor = con.cursor()
    query  = "CREATE TABLE ZeuS_" + yesterday + "(ioc VARCHAR(255), ioc_type VARCHAR(255), attack VARCHAR(255))"
    try:
        cursor.execute(query)
    except Exception as e:
        logging.debug(e)

    for line in data:
        line = line.strip()
        line = line.split('\t')

        ioc       = line[0]
        ioc_type  = line[1]
        attack    = line[2]

        query = "INSERT INTO ZeuS_" + yesterday + "(ioc, ioc_type, attack) VALUES(%s,%s,%s)"
        cursor.execute(query,(ioc, ioc_type, attack))

logging.info('save data...')
with open('data/zeus_' + yesterday + '.txt', 'w') as f:
    f.writelines(data)
