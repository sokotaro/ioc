#!/usr/bin/python
# -*- coding: utf-8 -*-
from OTXv2 import OTXv2
from pandas.io.json import json_normalize
from datetime import datetime, timedelta
import MySQLdb as mdb
import sys

con = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')

file = 'master.intel'

fileData = []

with con:
    cur = con.cursor()
    cur.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Alien_Vault_OTX'")
    create_time = cur.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time
    
    cmd = "RENAME TABLE Alien_Vault_OTX TO Alien_Vault_OTX" + create_time
    cur.execute(cmd)
    #-----------------------------------------------------#
    #-------------------ALIEN VAULT OTX-------------------#
    #-----------------------------------------------------#

#    cur.execute("DROP TABLE IF EXISTS Alien_Vault_OTX")
    cur.execute("CREATE TABLE Alien_Vault_OTX(Id INT PRIMARY KEY AUTO_INCREMENT, \
                 fields_indicator VARCHAR(256), indicator_type VARCHAR(32), meta_source VARCHAR(256), meta_url VARCHAR(256), do_notice VARCHAR(128))")

    f = open(file)
    i = 0

    print('Uploading Alien Vault OTX Data')
    for line in f:
#        print line
        data = line.split('\t')
        fileData.append(data)
        if ('#fields' not in line):
            cur.execute("INSERT INTO Alien_Vault_OTX(fields_indicator, indicator_type, meta_source, meta_url, do_notice) VALUES(%s,%s,%s,%s,%s)",(fileData[i][0],fileData[i][1],fileData[i][2],fileData[i][3],fileData[i][4],))
        i += 1
        if (i % 100) == 0:
            print('*'),

#    cur.execute("SELECT * FROM Alien_Vault_OTX")

#    rows = cur.fetchall()

#    for row in rows:
#        print row
    #-----------------------------------------------------#
    #-------------------ALIEN VAULT OTX-------------------#
    #-----------------------------------------------------#
    