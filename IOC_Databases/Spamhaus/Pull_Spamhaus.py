import locale
import MySQLdb as mdb
import time
import os

output = []
output2 = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    print 'Querying for ip addresses'
    cursor.execute("SELECT ip FROM Spamhaus_DROP")
	
    for ip in cursor:
        output.append(str(ip[0])+'\n')

    cursor.execute("SELECT ip FROM Spamhaus_EDROP")
	
    for ip in cursor:
        output2.append(str(ip[0])+'\n')

print 'Writing to File: Spamhaus_IP_Addresses'
with open('Spamhaus_DROP_IP_Addresses.txt', 'w') as f:
    f.writelines(output)
	
cmd = 'cat Spamhaus_DROP_IP_Addresses.txt | sort -u | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n > /home/katayama/Desktop/home/IOC_Databases/COMPARE_DATA/spamhaus_drop.txt'
print cmd
os.system(cmd)

with open('Spamhaus_EDROP_IP_Addresses.txt', 'w') as f:
    f.writelines(output2)
	
cmd = 'cat Spamhaus_EDROP_IP_Addresses.txt | sort -u | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n > /home/katayama/Desktop/home/IOC_Databases/COMPARE_DATA/spamhaus_edrop.txt'
print cmd
os.system(cmd)