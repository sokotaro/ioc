###########################################################
##                                                       ##
##  https://www.spamhaus.org/faq/section/Spamhaus%20DBL  ##
##                                                       ##
##  http://www.spamhaus.org/drop                         ##
##                                                       ##
###########################################################
from datetime import datetime, timedelta

import ipaddress
import locale
import MySQLdb as mdb
import time
import os
import datetime
import logging

create_time = str(datetime.date.today() - datetime.timedelta(days = 1))
create_time = create_time.replace('-','_')

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    cursor.execute("CREATE TABLE Spamhaus_EDROP_"+create_time+"(ioc VARCHAR(255), ioc_type VARCHAR(255), attack VARCHAR(255), sbl VARCHAR(255))")    
    # '''
    cmd = "mv edrop/edrop.txt edrop/edrop_"+create_time+".txt"
    logging.info((cmd))
    os.system(cmd)

    cmd = 'wget https://www.spamhaus.org/drop/edrop.txt -O edrop/edrop.txt'
    logging.info((cmd))
    os.system(cmd)
    filename = 'edrop/edrop.txt'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + str("{:,}".format(num_lines)))

    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        blob = f.read().splitlines()
    blob.pop(0)
    blob.pop(0)
    blob.pop(0)
    blob.pop(0)
    
    count = 0
    for line in blob:
        data = line.strip('\n')
        data = data.split(';')
        ips = data[0].strip()
        sbl = data[1]
        
#        net = ipaddress.ip_network(unicode(ip))
#        for a in net:
        cursor.execute("INSERT INTO Spamhaus_EDROP_"+create_time+"(ioc, ioc_type, attack, sbl) VALUES(%s,%s,%s,%s)",(ips, 'cidr', 'hijacked, bogon, spam', sbl))

        count += 1
        if(count % (num_lines/10) == 0):
            amt = ((10*count)/(num_lines/10))
            amt = str(amt)+'% | '
            logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'lines')
            
            
with con:
    cursor = con.cursor()

    cursor.execute("CREATE TABLE Spamhaus_DROP_"+create_time+"(ioc VARCHAR(255), ioc_type VARCHAR(255), attack VARCHAR(255), sbl VARCHAR(255))")    

    cmd = "mv drop/drop.txt drop/drop_"+create_time+".txt"
    logging.info((cmd))
    os.system(cmd)
    
    
    cmd = 'wget https://www.spamhaus.org/drop/drop.txt -O drop/drop.txt'
    logging.info((cmd))
    os.system(cmd)
    filename = 'drop/drop.txt'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + "{:,}".format(num_lines))

    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        blob = f.read().splitlines()
    blob.pop(0)
    blob.pop(0)
    blob.pop(0)
    blob.pop(0)
    
    count = 0
    for line in blob:
        data = line.strip('\n')
        data = data.split(';')
        ips = data[0].strip()
        sbl = data[1]
        
#        net = ipaddress.ip_network(unicode(ip))
#        for a in net:
        cursor.execute("INSERT INTO Spamhaus_DROP_"+create_time+"(ioc, ioc_type, attack, sbl) VALUES(%s,%s,%s,%s)", (ips, 'cidr', 'hijacked, bogon, spam', sbl))

        count += 1            
        if(count % (num_lines/10) == 0):
            amt = ((10*count)/(num_lines/10))
            amt = str(amt)+'% | '
            logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'lines')
