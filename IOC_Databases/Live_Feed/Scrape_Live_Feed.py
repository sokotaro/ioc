#!/usr/bin/python
# -*- coding: utf8 -*-
import sys
sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')

import MySQLdb as mdb
import time
import os

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
#latitude DECIMAL(10,8 )
#longitude DECIMAL(11, 8)
with con:
    cursor = con.cursor()
    cursor.execute("DROP TABLE IF EXISTS Live_Feed")
    cursor.execute("CREATE TABLE Live_Feed(status VARCHAR(16), attack VARCHAR(16) , victim_latitude VARCHAR(16), victim_longitude VARCHAR(16) , \
                    victim_country_code VARCHAR(8), victim_country_code2 VARCHAR(8), victim_city VARCHAR(128), victim_company VARCHAR(256), \
                    attacker_latitude VARCHAR(16), attacker_longitude VARCHAR(16), attacker_country_code VARCHAR(8), attacker_country_code2 VARCHAR(8), \
                    attacker_city VARCHAR(128), source VARCHAR(128), ip VARCHAR(16), port VARCHAR(8), protocol VARCHAR(128), type VARCHAR(24),\
                    id INT)")
    
    path = 'livefeed_data//'

    num_lines = 0
    for filename in os.listdir(path):
        tmp = sum(1 for line in open(path+filename))
        num_lines += tmp

    count = 0
    for filename in os.listdir(path):
        print 'Reading File: ', filename
        with open(path+filename) as f:
            for line in f:
                data = line.split('\t')
    #            print len(data)
    #            data = [x for x in data if x is not '']
    #            data = filter(None, data)
    #            print data
                if len(data) != 19:
                    print 'Error: ', data
                    raw_input()
                else:
                    status = data[0]
                    attack = data[1]
                    victim_latitude = data[2]
                    victim_longitude = data[3]
                    victim_country_code = data[4]
                    victim_country_code2 = data[5]
                    victim_city = data[6]
                    victim_company = data[7]
                    attacker_latitude = data[8]
                    attacker_longitude = data[9]
                    attacker_country_code = data[10]
                    attacker_country_code2 = data[11]
                    attacker_city = data[12]
                    source = data[13]
                    ip = data[14]
                    port = data[15]
                    protocol = data[16]
                    type = data[17]
                    id = data[18]            
                    id = id.strip('\n')
                    

                cursor.execute("INSERT INTO Live_Feed(status, attack, victim_latitude, victim_longitude, victim_country_code, victim_country_code2, \
                                                        victim_city, victim_company, attacker_latitude, attacker_longitude, attacker_country_code, \
                                                        attacker_country_code2, attacker_city, source, ip, port, protocol, type, id) \
                                                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", \
                                                        (status, attack, victim_latitude, victim_longitude, victim_country_code, victim_country_code2, \
                                                        victim_city, victim_company, attacker_latitude, attacker_longitude, attacker_country_code, \
                                                        attacker_country_code2, attacker_city, source, ip, port, protocol, type, id,))
                count += 1            
                if(count % (num_lines/100) == 0):
                    print 'Processed: ', 
                    amt = ((1*count)/(num_lines/100))
                    amt = str(amt)+'% | '
                    print amt,
                    print "{:,}".format(count),
                    print 'lines'
