import sys
'''
sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')
'''

import locale
import MySQLdb as mdb
import time
import os

output = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    print 'Querying for ip addresses'
    cursor.execute("SELECT ip FROM Talos_Intel")
	
    for ip in cursor:
        output.append(str(ip[0])+'\n')

print 'Writing to File: Talos_Intel_IP_Addresses'
with open('Talos_Intel_IP_Addresses.txt', 'w') as f:
    f.writelines(output)
	
cmd = 'cat Talos_Intel_IP_Addresses.txt | sort -u | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n > /home/katayama/Desktop/home/IOC_Databases/COMPARE_DATA/talos_intel.txt'
print cmd
os.system(cmd)