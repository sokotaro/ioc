#######################################################
##                                                   ##
##  http://www.talosintel.com/additional-resources/  ##
##                                                   ##
#######################################################
import sys
from datetime import datetime, timedelta

import locale
import MySQLdb as mdb
import time
import os
import datetime
import logging


yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Talos_Intel'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info(create_time)
    
    try:
        cmd = "RENAME TABLE Talos_Intel TO Talos_Intel_" + yesterday
        cursor.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass
    
    # cursor.execute("DROP TABLE IF EXISTS Talos_Intel")
    
    cursor.execute("CREATE TABLE Talos_Intel(ip VARCHAR(16) NOT NULL)")    
    # '''
    cmd = "mv ip-filter.blf ip-filter"+create_time+".blf"
    logging.info((cmd))
    os.system(cmd)
    
    cmd = 'wget http://www.talosintel.com/feeds/ip-filter.blf'
    logging.info((cmd))
    os.system(cmd)
    filename = 'ip-filter.blf'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + str("{:,}".format(num_lines)))

    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        blob = f.read().splitlines()
    
    count = 0
    for line in blob:
        data = line.strip('\n')
        ip = str(data)
        
#        print ip
#        print type(ip)
        cursor.execute("INSERT INTO Talos_Intel(ip) VALUES(%s)",[ip])

        count += 1            
        if(count % (num_lines/10) == 0):
            amt = ((10*count)/(num_lines/10))
            amt = str(amt)+'% | '
            logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'lines')
