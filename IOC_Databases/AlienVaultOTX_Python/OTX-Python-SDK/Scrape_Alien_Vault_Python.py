from OTXv2 import OTXv2, IndicatorTypes
from pandas.io.json import json_normalize
from datetime import datetime, timedelta
import MySQLdb as mdb
import datetime, logging
# encoding=utf8

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

# AlienVaultOTX Python Connector
logging.info('Getting all pulse data...')
try:
    otx        = OTXv2("0df1996cb4a5ccfeab82bffac382b244f04f192f249cff1753d2e9e0caab684a")
    pulses     = otx.getall()
    num_pulses = len(pulses)
except Exception as e:
    logging.debug((e.message))
    logging.debug((e.args))
    raise
    
logging.info("num_pulses = " + str(num_pulses))

# MySQL DB Connector
logging.info('\nConnecting to OpenIOC database\n')
con        = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Alien_Vault_OTX_Python'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info((create_time))

    cmd = "RENAME TABLE Alien_Vault_OTX_Python TO Alien_Vault_OTX_Python_" + yesterday
    try:
        cursor.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass

    try:            
        cursor.execute("CREATE TABLE Alien_Vault_OTX_Python(pulse_id VARCHAR(255), created VARCHAR(255), author VARCHAR(255), reference VARCHAR(255), indicator VARCHAR(255), type VARCHAR(255), name VARCHAR(255), tags VARCHAR(255), description VARCHAR(255))")
    except Exception as e:
        logging.debug(e)
        pass

    count = 0
    for i in range(num_pulses):
        tmp  = pulses[i]['indicators']
        name = str(pulses[i]['name'].encode('utf-8'))
        try:
            tags = ', '.join(pulses[i]['tags']).encode('utf-8')
        except:
            try:
                tags = str(pulses[i]['tags']).encode('utf-8')
            except:
                tags = ' '
        try:
            author = ', '.join(pulses[i]['author_name']).encode('utf-8')
        except:
            try:
                author = str(pulses[i]['author_name']).encode('utf-8')
            except:
                author = ' '
        try:
            references = ', '.join(pulses[i]['references']).encode('utf-8')
        except:
            try:
                references = str(pulses[i]['references']).encode('utf-8')
            except:
                references = ' '
                
        # print str(i) + ' | ',
        for j in range(len(tmp)):
            try:
                pulse_id    = str(tmp[j]['_id'])
            except:
                pulse_id    = ' '
            if pulse_id == ' ':
                try:
                    pulse_id = str(tmp[j]['id'])
                except:
                    pulse_id = ' '
            try:
                indicator   = str(tmp[j]['indicator'].encode('utf-8'))
            except:
                indicator   = ' '
            try:
                ptype       = str(tmp[j]['type'])
            except:
                ptype       = ' '
            try:
                created     = str(tmp[j]['created'])
            except:
                created     = ' '
            try:
                description = str(tmp[j]['description'].encode('utf-8'))
            except:
                description = ' '

            try:
                cursor.execute("INSERT INTO Alien_Vault_OTX_Python(pulse_id, created, author, reference, indicator, type, name, tags, description) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)",(pulse_id, created, author, references, indicator, ptype, name, tags, description))
            except Exception as e:
                print e
                pass
        count += 1
        if(count % (num_pulses/10) == 0):
            amt = ((10*count)/(num_pulses/10))
            amt = str(amt)+'% | '
            logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'feeds\n')

