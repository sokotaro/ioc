import sys
import locale
import MySQLdb as mdb
import time
import os
import MySQLdb as mdb
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday.replace('-','_')+'.log',level=logging.DEBUG)

#    cmd = 'wget http://lists.blocklist.de/lists/all.txt'
cmd = 'wget https://lists.blocklist.de/lists/dnsbl/all.list -O Blocklist_DE_all_' + yesterday + '.list'
logging.info(cmd)
os.system(cmd)

data = []
with open('Blocklist_DE_all_' + yesterday + '.list') as f:
    for line in f:
        line = line.strip()
        if '127.0.0' not in line:
            data.append(line)
        elif len(line.split(':')) > 1:
            if ':' not in line[0]:
                data.append(line)
total = len(data)

yesterday = yesterday.replace('-','_')

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()    
    query = "CREATE TABLE Blocklist_DE_IP_" + yesterday + "(ip VARCHAR(16) NOT NULL, host VARCHAR(255), last_attack VARCHAR(255), description VARCHAR(255))"
    try:
        cursor.execute(query)
    except Exception as e:
        logging.debug(e)

    count = 0
    for line in data:
        line = line.split(':', 2)
        
        if len(line) > 1:
            ip          = line[0]
            host        = line[1]
            last_attack = line[2].split('Last-Attack: ')[1].split()[0].strip('),')
            last_attack = datetime.datetime.fromtimestamp(float(last_attack)).strftime('%Y-%m-%d %H:%M:%S')
            description = line[2]
        else:
            ip          = line[0]
            host        = ''
            last_attack = ''
            description = ''

        query = "INSERT INTO Blocklist_DE_IP_" + yesterday + "(ip, host, last_attack, description) VALUES(%s,%s,%s,%s)"
        cursor.execute(query,(ip, host, last_attack, description))
        
        count += 1
        if (count % (total/10)) == 0:
            logging.info('Processed: ' + str((10*count)/(total/10)) + '% | ' + str('{:,}'.format(count)) + ' ip-blocks')
