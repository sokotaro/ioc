import sys
'''
sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')
'''

import locale
import MySQLdb as mdb
import time
import os

output = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    print 'Querying for ip addresses'
    cursor.execute("SELECT ip FROM Charles_Haley_SSH")
	
    for ip in cursor:
        output.append(str(ip[0])+'\n')

print 'Writing to File: Charles_Haley_SSH_IP_Addresses'
with open('Charles_Haley_SSH_IP_Addresses.txt', 'w') as f:
    f.writelines(output)
	
cmd = 'cat Charles_Haley_SSH_IP_Addresses.txt | sort -u > /home/katayama/Desktop/home/IOC_Databases/COMPARE_DATA/charles_haley.txt'
print cmd
os.system(cmd)
