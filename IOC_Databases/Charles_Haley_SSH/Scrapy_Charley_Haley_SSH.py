# coding: utf-8  
import unicodedata 
import sys
import urllib2
import datetime
import os
import locale
import MySQLdb as mdb
import logging

from bs4 import BeautifulSoup

def removeNonAscii(s): 
    return "".join(i for i in s if ord(i)<128)

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

url = "http://charles.the-haleys.org/ssh_dictionary_attacks.php"  # change to whatever your url is

page = urllib2.urlopen(url).read()
soup = BeautifulSoup(page)

web_data = soup.findAll('td')

data = []
date = '_'+str(datetime.datetime.fromtimestamp(os.path.getmtime('hostdeny_scrape_PHP.txt'))).split()[0]

cmd = 'mv hostdeny_scrape_PHP.txt hostdeny_scrape_PHP'+date+'.txt'
logging.info(cmd)
os.system(cmd)

with open('hostdeny_scrape_PHP.txt', 'w') as f:
    for line in web_data:
        line = str(line)
        line = line.replace('<td>','')
        line = line.replace('</td>','')
        if len(line):
            data.append(line + '\n')
    f.writelines(data)



print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
#latitude DECIMAL(10,8 )
#longitude DECIMAL(11, 8)
with con:
    cursor = con.cursor()
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Charles_Haley_SSH'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info((create_time))
    
    cmd = "RENAME TABLE Charles_Haley_SSH TO Charles_Haley_SSH_" + yesterday
    try:
        cursor.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass
#    cursor.execute("DROP TABLE IF EXISTS Charles_Haley_SSH")
    cursor.execute("CREATE TABLE Charles_Haley_SSH(ip VARCHAR(16) NOT NULL,\
                    date_reported VARCHAR(10) NOT NULL DEFAULT '0')")
    
    filename = 'hostdeny_scrape_PHP.txt'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + str("{:,}".format(num_lines)))
    
    
    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        blob = f.readlines()
    
    blob.pop(0)
    count = 0
    for line in blob:
        data = line.strip()
        data = data.split('\xc2\xa0')
        ip = data[0]
        date_reported = data[1]

        cursor.execute("INSERT INTO Charles_Haley_SSH(ip, date_reported) VALUES(%s,%s)",(ip, date_reported))

        count += 1            
        if(count % (num_lines/10) == 0):
            amt = ((10*count)/(num_lines/10))
            amt = str(amt)+'% | '
            logging.info('Processed: ' + amt + str("{:,}".format(count)) + 'lines')
