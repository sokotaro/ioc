import sys

if (len(sys.argv)) != 3:
    print 'Usage: python compare.py set_1.txt set_2.txt'
    exit(1)

file_1 = sys.argv[1]
file_2 = sys.argv[2]

set_1 = []
set_2 = []

print 'Reading: ', file_1
with open(file_1) as f:
    for line in f:
        set_1.append(line)
        
print 'Reading: ', file_2
with open(file_2) as f:
    for line in f:
        set_2.append(line)

print 'Searching for Matches'
Matches = list(set(set_1).intersection(set_2))

'''
print 'Saving Matches\n'
with open(file_1+'_vs_'+file_2, 'w') as f:
    f.writelines(Matches)
'''

print '%62s' % file_1,
print ' | ',
print '%10s' % "{:,}".format(len(set_1))

print '%62s' % file_2,
print ' | ',
print '%10s' % "{:,}".format(len(set_2))

print '%62s' % 'Matches',
print ' | ',
print '%10s' % "{:,}".format(len(Matches))

print '\n'

if (len(set_1) > len(set_2)):
    print '%62s' % 'Overlap',
    print ' | ',
    print '%10s' % "{0:.0f}%".format(float(len(Matches))/float(len(set_2)) * 100)
else:
    print '%62s' % 'Overlap',
    print ' | ',
    print '%10s' % "{0:.0f}%".format(float(len(Matches))/float(len(set_1)) * 100)
