#!/usr/bin/env python

import argparse
import re
import socket
# Requires dnspython AKA python-dns package
import dns.resolver
from urllib2 import urlopen

bls = ["b.barracudacentral.org", "cdl.anti-spam.org.cn", "cidr.bl.mcafee.com"]
# bls = ["b.barracudacentral.org", "bl.spamcannibal.org", "bl.spamcop.net",
       # "blacklist.woody.ch", "cbl.abuseat.org", "cdl.anti-spam.org.cn",
       # "combined.abuse.ch", "combined.rbl.msrbl.net", "db.wpbl.info",
       # "dnsbl-1.uceprotect.net", "dnsbl-2.uceprotect.net",
       # "dnsbl-3.uceprotect.net", "dnsbl.cyberlogic.net",
       # "dnsbl.sorbs.net", "drone.abuse.ch", "drone.abuse.ch",
       # "duinv.aupads.org", "dul.dnsbl.sorbs.net", "dul.ru",
       # "dyna.spamrats.com", "dynip.rothen.com",
       # "http.dnsbl.sorbs.net", "images.rbl.msrbl.net",
       # "ips.backscatterer.org", "ix.dnsbl.manitu.net",
       # "korea.services.net", "misc.dnsbl.sorbs.net",
       # "noptr.spamrats.com", "ohps.dnsbl.net.au", "omrs.dnsbl.net.au",
       # "orvedb.aupads.org", "osps.dnsbl.net.au", "osrs.dnsbl.net.au",
       # "owfs.dnsbl.net.au", "pbl.spamhaus.org", "phishing.rbl.msrbl.net",
       # "probes.dnsbl.net.au", "proxy.bl.gweep.ca", "rbl.interserver.net",
       # "rdts.dnsbl.net.au", "relays.bl.gweep.ca", "relays.nether.net",
       # "residential.block.transip.nl", "ricn.dnsbl.net.au",
       # "rmst.dnsbl.net.au", "smtp.dnsbl.sorbs.net",
       # "socks.dnsbl.sorbs.net", "spam.abuse.ch", "spam.dnsbl.sorbs.net",
       # "spam.rbl.msrbl.net", "spam.spamrats.com", "spamrbl.imp.ch",
       # "t3direct.dnsbl.net.au", "tor.dnsbl.sectoor.de",
       # "torserver.tor.dnsbl.sectoor.de", "ubl.lashback.com",
       # "ubl.unsubscore.com", "virus.rbl.jp", "virus.rbl.msrbl.net",
       # "web.dnsbl.sorbs.net", "wormrbl.imp.ch", "xbl.spamhaus.org",
       # "zen.spamhaus.org", "zombie.dnsbl.sorbs.net"]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Is This IP Bad?')
    parser.add_argument('-i', '--ip', help='IP address to check')
    parser.add_argument('--success', help='Also display GOOD', required=False, action="store_true")
    args = parser.parse_args()

    badip = args.ip

    #IP INFO
    reversed_dns = socket.getfqdn(badip)
    # geoip = urllib.urlopen('http://api.hackertarget.com/geoip/?q='
                           # + badip).read().rstrip()

    # print(blue('\nThe FQDN for {0} is {1}\n'.format(badip, reversed_dns)))
    # print(red('Geolocation IP Information:'))
    # print(blue(geoip))
    # print('\n')

    BAD = 0
    GOOD = 0
    
    BAD = BAD
    GOOD = GOOD

    for bl in bls:
        try:
            my_resolver = dns.resolver.Resolver()
            query = '.'.join(reversed(str(badip).split("."))) + "." + bl
            my_resolver.timeout = 5
            my_resolver.lifetime = 5
            # answers = my_resolver.query(query, "A")
            answer_txt = my_resolver.query(query, "TXT")
            # print (red(badip + ' is listed in ' + bl)
                   # + ' (%s: %s)' % (answers[0], answer_txt[0]))
            BAD = BAD + 1

        except dns.resolver.NXDOMAIN:
            # print (green(badip + ' is not listed in ' + bl))
            GOOD = GOOD + 1

        # except dns.resolver.Timeout:
            # print (blink('WARNING: Timeout querying ' + bl))

        # except dns.resolver.NoNameservers:
            # print (blink('WARNING: No nameservers for ' + bl))

        # except dns.resolver.NoAnswer:
            # print (blink('WARNING: No answer for ' + bl))

    print BAD
    # print(red('\n{0} is on {1}/{2} blacklists.\n'.format(badip, BAD, (GOOD+BAD))))
