import MySQLdb as mdb
import GeoIP
import math
import os
import gc

def read(file):
    return set(map(str.strip, open(file)))
    
gi = GeoIP.open("/usr/local/share/GeoIP/GeoLiteCity.dat", GeoIP.GEOIP_STANDARD)

universe = read("UNIVERSE.TXT")
alien    = read("alien_vault.txt")
anti     = read("anti_hacker_alliance.txt")
block    = read("blocklist_de.txt")
charles  = read("charles_haley.txt")
critical = read("critical_stack.txt")
feodo    = read("feodo.txt")
live     = read("live_feed.txt")
long     = read("long_tail.txt")
palevo   = read("palevo.txt")
ransom   = read("ransom_ware.txt")
drop     = read("spamhaus_drop.txt")
edrop    = read("spamhaus_edrop.txt")
talos    = read("talos_intel.txt")
tor      = read("tor_Addresses.txt")
zeus     = read("zeus.txt")

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
#latitude DECIMAL(10,8 )
#longitude DECIMAL(11, 8)
with con:
    cursor = con.cursor()

    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'IP_rankings'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time

    cmd = "RENAME TABLE IP_rankings TO IP_rankings" + create_time
    cursor.execute(cmd)
    
    # cursor.execute("DROP TABLE IF EXISTS IP_rankings")
    cursor.execute("CREATE TABLE IP_rankings(ip VARCHAR(32) NOT NULL,\
                    rank DOUBLE NOT NULL,\
                    country CHAR(128) NOT NULL,\
                    region CHAR(128) NOT NULL,\
                    city CHAR(32) NOT NULL)")
                    
    out = []    
    count = 0
    num_lines = len(universe)

    print 'num_lines = ', "{:,}".format(num_lines)

    for ip in universe:
        gc.disable()
        ip = {ip}
        # print ip
        hit = 0
        
        if (ip).intersection(alien):
            hit += 1

        if (ip).intersection(anti):
            hit += 1

        if (ip).intersection(block):
            hit += 1

        if (ip).intersection(charles):
            hit += 1

        if (ip).intersection(critical):
            hit += 1

        if (ip).intersection(feodo):
            hit += 1

        if (ip).intersection(live):
            hit += 1

        if (ip).intersection(long):
            hit += 1
        
        if (ip).intersection(palevo):
            hit += 1
        
        if (ip).intersection(ransom):
            hit += 1
        
        if (ip).intersection(drop):
            hit += 1
        
        if (ip).intersection(edrop):
            hit += 1
        
        if (ip).intersection(talos):
            hit += 1
        
        if (ip).intersection(tor):
            hit += 1

        if (ip).intersection(zeus):
            hit += 1

        rank = 1.0 + float((16.0)*(math.log(float(hit))/math.log(1.522)))
    #    rank = hit
        # print rank
        ip = ip.pop()
        gir = gi.record_by_addr(ip)
        if (gir):
            if gir['country_name']:
                country = gir['country_name']
            else:
                country = ''

            if gir['region_name']:
                region = gir['region_name']
            else:
                region = ''

            if gir['city']:
                city = gir['city']
            else:
                city = ''
            cursor.execute("INSERT INTO IP_rankings(ip, rank, country, region, city) VALUES(%s,%s,%s,%s,%s)",(ip, rank, country, region, city))
            # out.append(ip + '\t' + str(rank) + '\t' + country + '\t' + region + '\t' + city + '\n')
        else:
            cursor.execute("INSERT INTO IP_rankings(ip, rank, country, region, city) VALUES(%s,%s,%s,%s,%s)",(ip, rank, ' ', ' ', ' '))
            # out.append(ip + '\t' + str(rank) + '\n')
    #    print out
        # break
        '''
        cmd = "python tmp/ipcheck.py -i "+ip+" > tmp.txt"
        os.system(cmd)
        
        with open("tmp/tmp.txt") as f:
            data = f.read().splitlines()
        
        country = data[1]
        state = data[2]
        city = data[3]
        lat = data[4]
        long = data[5]
        print "hit old =",hit
        hit += int(data[6])
        print "hit new =",hit
        '''
    #    print count
        count += 1            
        if(count % (num_lines/100) == 0):
            print 'Processed: ', 
            amt = ((1*count)/(num_lines/100))
            amt = str(amt)+'% | '
            print amt,
            print "{:,}".format(count),
            print 'lines'
        gc.enable()
# with open("IP_rankings.txt", 'w') as f:
    # f.writelines(out)
