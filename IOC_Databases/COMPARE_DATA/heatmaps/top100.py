import locale
import MySQLdb as mdb
import time
import os

output = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'top_100'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time
    
    cmd = "RENAME TABLE top_100 TO top_100" + create_time
    cursor.execute(cmd)
    
    print 'Querying for ip addresses'
    cursor.execute("CREATE TABLE top_100(ip VARCHAR(32) NOT NULL,\
                    rank DOUBLE NOT NULL,\
                    country CHAR(128) NOT NULL,\
                    region CHAR(128) NOT NULL,\
                    city CHAR(32) NOT NULL)")

    cursor.execute("select * from IP_rankings order by rank desc limit 100")
	
    for (ip, rank, country, region, city) in cursor:
        cursor.execute("INSERT INTO top_100(ip, rank, country, region, city) VALUES(%s,%s,%s,%s,%s)",(ip, rank, country, region, city))
