import locale
import MySQLdb as mdb
import time
import os
import gc

output = []

print 'Connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    
    print 'Querying for ip addresses'
    cursor.execute("select ip, rank from IP_rankings order by INET_ATON(ip)")# rank desc")

    count = 0
    num_lines = cursor.rowcount

    print 'num_lines = ', "{:,}".format(num_lines)    
	
    for (ip, rank) in cursor:
        gc.disable()
        if (':' not in ip) and ('.' in ip):
            for i in xrange(0,int(rank)):
                output.append(ip+'\n')

        count += 1            
        if(count % (num_lines/100) == 0):
            print 'Processed: ', 
            amt = ((1*count)/(num_lines/100))
            amt = str(amt)+'% | '
            print amt,
            print "{:,}".format(count),
            print 'lines'
        gc.enable()

print 'saving...'
with open('tmp.txt', 'w') as f:
    f.writelines(output)

'''    
print 'sorting...'
cmd = 'cat tmp.txt | sort -t . -k 1,1n -k 2,2n -k 3,3n -k 4,4n > all.txt'
print cmd
os.system(cmd)
'''