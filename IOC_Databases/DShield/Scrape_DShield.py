import MySQLdb as mdb
import datetime
import netaddr
import os
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))

#yesterday = '2016-07-19'
with open('monitor/monitor_'+yesterday) as f:
    data = f.readlines()
#  MySQL does not like '-'
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    query = "CREATE TABLE DShield_" + yesterday + "(ip VARCHAR(16), attacks VARCHAR(255), network_name VARCHAR(255), country VARCHAR(255), contact_email VARCHAR(255))"
    
    try:
        cursor.execute(query)
    except Exception as e:
        logging.debug(e)

    for line in data:
        line = line.strip()
        if ('#' not in line) and ('Start' not in line) and (line != ''):
            line = line.split('\t')
            # print 'processing: ' + line[0]
            # print line

            if len(line) > 6:
                start         = line[0]
                end           = line[1]
                netblock      = line[2]
                attacks       = line[3]
                network_name  = line[4]
                country       = line[5]
                contact_email = line[6]
                #  print [start,end,netblock,attacks,network_name,country,contact_email]
            if len(line) == 5:
                start         = line[0]
                end           = line[1]
                netblock      = line[2]
                attacks       = line[3]
                network_name  = line[4]
                country       = line[5]
                contact_email = 'N/A'
            if len(line) == 4:
                start         = line[0]
                end           = line[1]
                netblock      = line[2]
                attacks       = line[3]
                network_name  = 'N/A'
                country       = 'N/A'
                contact_email = 'N/A'
                #  print [start,end,netblock,attacks,network_name,country,contact_email]

            ips = netaddr.IPRange(start, end)
            for ip in ips:
                ip = str(ip)
                query = "INSERT INTO DShield_" + yesterday + "(ip, attacks, network_name, country, contact_email) VALUES(%s,%s,%s,%s,%s)"
                cursor.execute(query,(ip, attacks, network_name, country, contact_email))
