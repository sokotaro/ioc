###########################################################
##                                                       ##
##         https://github.com/vz-risk/VCDB/              ##
##                                                       ##
##         VERIS Community DB                            ##
##                                                       ##
###########################################################

import sys
from datetime import datetime, timedelta

import locale
import MySQLdb as mdb
import time
import csv
import os

# date = '_'+str(datetime.datetime.fromtimestamp(os.path.getmtime('veris.csv'))).split()[0]
# cmd = 'mv veris.csv veris.csv'+date+'.csv'

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    '''
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
    WHERE table_schema = 'openioc' \
    AND table_name = 'Feodo'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time

    cmd = "RENAME TABLE Feodo TO Feodo" + create_time
    cursor.execute(cmd)

    # cursor.execute("DROP TABLE IF EXISTS Talos_Intel")
    '''
    with open('header') as f:
        columns = f.read()
    columns = columns.replace('.', '_').replace('-','_').replace('/','_')

    temp = columns.split(' VARCHAR(25),')
    columns = []
    for line in temp:
        if 'pattern_Everything_Else' in line:
            columns.append(line[:64])
        else:
            columns.append(line[:64]+ ' VARCHAR(25),')
        
    columns = ''.join(columns)
#    print columns

    query = "CREATE TABLE VERIS_Community_DB(" + columns  + ")"
    print "\n*******************************\n"
#    print query
    cursor.execute(query)

'''
cmd = "mv veris.csv veris"+create_time+".csv"
print cmd
os.system(cmd)
'''
cmd ="wget https://github.com/vz-risk/VCDB/blob/master/data/csv/vcdb.csv?raw=true -O veris.csv"
print cmd
os.system(cmd)

num_lines = sum(1 for line in open(filename)) - 1
print 'num_lines =', "{:,}".format(num_lines)

csv_data = csv.reader(file('veris.csv'))
csv_data.pop(0)

count = 0
for row in csv_data:
    cursor.execut("INSERT INTO VERIS_Community_DB() VALUES()",)

    count += 1
    if(count % (num_lines/10) == 0):
        print 'Processed: ',
        amt = ((10*count)/(num_lines/10))
        amt = str(amt)+'% | '
        print amt,
        print "{:,}".format(count),
        print 'lines'
    
