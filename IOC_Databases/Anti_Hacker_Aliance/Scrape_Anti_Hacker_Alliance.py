#!/usr/bin/python
import locale
import MySQLdb as mdb
import time
import os
from datetime import datetime, timedelta
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')
#latitude DECIMAL(10,8 )
#longitude DECIMAL(11, 8)
with con:
    cursor = con.cursor()
    
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
		    WHERE table_schema = 'openioc' \
		    AND table_name = 'Anti_Hacker_Alliance_Black_List_365'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info((create_time))

    cmd = "RENAME TABLE Anti_Hacker_Alliance_Black_List_365 TO Anti_Hacker_Alliance_Black_List_365_" + yesterday
    try:
        cursor.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass

    try:
        cursor.execute("CREATE TABLE Anti_Hacker_Alliance_Black_List_365(ip VARCHAR(16) NOT NULL,\
                    reported int(4) NOT NULL DEFAULT '0',\
                    date_time CHAR(19) NOT NULL DEFAULT '0')")
    except Exception as e:
        logging.debug(e)
        pass

    # cmd = "mv Black_list_ip_365.txt Black_list_ip_365_"+create_time+".txt"
    cmd = "tar -zcvf Black_list_ip_365_"+create_time+".tar.gz Black_list_ip_365.txt"
    logging.info((cmd))
    os.system(cmd)
    
    # http://anti-hacker-alliance.com/payguard/ipn-master/dl.php?id=4L607322WK336003F&file=4965
    # cmd = 'curl \"http://anti-hacker-alliance.com/payguard/ipn-master/dl.php?id=84J16942RS9977232&file=4965\" -o \"Black_list_ip_365.txt.bz2\"'
    cmd = 'curl \"http://anti-hacker-alliance.com/payguard/ipn-master/dl.php?id=4L607322WK336003F&file=4965\" -o \"Black_list_ip_365.txt.bz2\"'
    logging.info((cmd))
    os.system(cmd)

    cmd = 'bzip2 -dk Black_list_ip_365.txt.bz2'
    logging.info((cmd))
    os.system(cmd)
    
    filename = 'Black_list_ip_365.txt'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + str("{:,}".format(num_lines)))
    
    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        
        count = 0
        for line in f:
            data = line.replace('\"', '')
            data = data.strip('\r\n')
            data = data.split(',')
            ip = data[0]
            reported = data[1]
            date_time = data[2]

            cursor.execute("INSERT INTO Anti_Hacker_Alliance_Black_List_365(ip, reported, date_time) VALUES(%s,%s,%s)",(ip, reported, date_time))

            count += 1            
            if(count % (num_lines/100) == 0):
                amt = ((1*count)/(num_lines/100))
                amt = str(amt)+'% | '
                logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'lines')

