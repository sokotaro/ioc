import os
import re
import json
import urllib2
import netaddr
import requests
import ipaddress
from bs4 import BeautifulSoup
from ipwhois import IPWhois
from datetime import datetime, timedelta

def kaseya():
    captured = datetime.today().date().isoformat()
    
    print 'scraping ip\'s from kaseya'
    url  = 'https://community.kaseya.com/kb-legacy/w/wiki/895.kaseya-cloud-ip-addresses-and-ports.aspx'
    data = requests.get(url)
    soup = BeautifulSoup(data.text, 'lxml')
    cmd  = 'wget ' + url + ' -O kaseya-src_' + captured

    print cmd
    os.system(cmd)

    print 'processing ip\'s'
    count  = 0
    output = []
    total  = len(soup.find_all('li'))

    for line in soup.find_all('li'):
        line = line.getText()
        if re.search("^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}", line):
            if '-' in line:
                line = line.replace(' ','').split('-')
                ips = netaddr.IPRange((line[0]),(line[0][:-1]+line[1]))
                print ips

                for ip in ips:
                    ip     = str(ip)
                    obj    = IPWhois(ip)
                    result = obj.lookup_whois(retry_count=1)
#                    print ip
                    
                    output.append(ip + '\t' + str(result['nets'][0]['updated']) + '\n')
            else:
                line   = str(line)
                obj    = IPWhois(line)
                result = obj.lookup_whois(retry_count=1)
#                print line

                output.append(line + '\t' + str(result['nets'][0]['updated']) + '\n')

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' ip\'s'

    with open('kaseya_' + captured, 'w') as f:
        f.writelines(output)
    
def amazon():
    captured = datetime.today().date().isoformat()

    print 'scraping ip\'s from amazon aws'
    url  = 'https://ip-ranges.amazonaws.com/ip-ranges.json'
    data = json.load(urllib2.urlopen(url))
    cmd  = 'wget ' + url + ' -O amazon-src_' + captured

    print cmd
    os.system(cmd)

    print 'processing ip\'s'
    count  = 0
    output = []
    total  = len(data['prefixes'])

    for line in data['prefixes']:
        prefix = str(line['ip_prefix'])
        print prefix
        
        for ip in netaddr.IPNetwork(prefix):
            ip     = str(ip)
            obj    = IPWhois(ip)
            result = obj.lookup_whois(retry_count=1)

            output.append(ip + '\t' + str(result['nets'][0]['updated']) + '\n')

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' ip\'s'


    with open('amazon_' + captured, 'w') as f:
        f.writelines(output)




#kaseya()
amazon()
