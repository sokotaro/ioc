import datetime
import MySQLdb as mdb

print 'connecting to openioc database...'
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    
    query = "SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Banned_Hacker_IPs'"
    cursor.execute(query)

    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    query = "RENAME TABLE Banned_Hacker_IPs TO Banned_Hacker_IPs" + create_time
    cursor.execute(query)
    
    query = 'CREATE TABLE Banned_Hacker_IPs(ip VARCHAR(16), date_reported VARCHAR(10), feed VARCHAR(32))'
    cursor.execute(query)

    with open('banned') as f:
        data = f.readlines()

    total = len(data)
    count = 0

    for line in data:
        line           = line.strip().split()
        ip             = line[0]
        date_reported  = line[1]
        feed           = line[2]
        query          = 'INSERT INTO Banned_Hacker_IPs(ip, date_reported, feed) VALUES(%s,%s,%s)'
        cursor.execute(query,(ip, date_reported, feed))

        count += 1
        if (count % (total/10)) == 0:
            print 'Processed: ' + str((10*count)/(total/10)) + '% | ' + '{:,}'.format(count) + ' IOCs'

