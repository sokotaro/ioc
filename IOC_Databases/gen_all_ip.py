
from netaddr import *

size = 4294967295/100
for j in range(100):
    percent = 0
    out = []

    with open('all_possible_ips_'+str(j)+'_.txt', 'w') as f:
        print 'Generating IPs'
        for i in range(j*size, (j+1)*size):
            out.append(str(IPAddress(i)) + '\n')
            if i % (size / 100) == 0:
                print 'percent: ', percent
                percent += 1
        print '\nFlushing data to file'
        f.writelines(out)