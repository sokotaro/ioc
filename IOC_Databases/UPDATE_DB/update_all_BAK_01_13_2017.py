import os

### Alien Vault OTX ###
###################################################################################################
#                                                                                                 #
#   Prior to running this, execute AlienVaultOTX_subscribe.py to subscribe to all latest feeds    #
#                                                                                                 #
###################################################################################################
print '\nAlien Vault OTX\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/AlienVaultOTX_Python/OTX-Python-SDK/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Alien_Vault_Python.py'
print cmd
os.system(cmd)

print '\nAlien Vault OTX\n'

''' THIS IS THE OLD WAY - DOES NOT INCLUDE CREATE TIME
cmd = '/home/katayama/Desktop/home/IOC_Databases/AlienVaultOTX/OTX-Apps-Bro-IDS-master/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'python otxv2-bro.py check_new'
print cmd
os.system(cmd)

cmd = 'rsync -av /nsm/bro/share/bro/site/OTX-Apps-Bro-IDS/pulses /home/katayama/Desktop/home/IOC_Databases/AlienVaultOTX/OTX-Apps-Bro-IDS-master/pulses'
print cmd
os.system(cmd)

cmd ='pulses'
print 'cd ' + cmd
os.chdir(cmd)

cmd ='python CombineAlienVaultIntel.py > master.intel'
print cmd
os.system(cmd)

cmd ='ipython ScrapeAlienVault.py'
print cmd
os.system(cmd)

cmd ='ipython Pull_IP_AlienVaultOTX.py'
print cmd
os.system(cmd)
'''
### Alien Vault OTX ###


### Critical Stack ###
print '\nCritical Stack\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Critical_Stack/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'critical-stack-intel list'
print cmd
os.system(cmd)

cmd = 'critical-stack-intel pull'
print cmd
os.system(cmd)

#cmd = 'rsync -av /opt/critical-stack/frameworks/intel/master-public.bro.dat master-public.bro.dat'
cmd = 'cp -f /opt/critical-stack/frameworks/intel/master-public.bro.dat master-public.bro.dat'
print cmd
os.system(cmd)

cmd ='ipython ScrapeCriticalStack.py'
print cmd
os.system(cmd)

'''
cmd ='ipython Pull_IP_Critical_Stack.py'
print cmd
os.system(cmd)
'''
print '\nCritical Stack\n'
### Critical Stack ###

### Charles Haley SSH ###
print '\nCharles Haley\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Charles_Haley_SSH/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrapy_Charley_Haley_SSH.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_IP_Charley_Haley_SSH.py'
print cmd
os.system(cmd)
'''
print '\nCharles Haley\n'
### Charles Haley SSH ###

### Blocklist DE ###
print '\nBlocklist DE\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Blocklist_DE/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Blocklist_DE.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_Blocklist_DE.py'
print cmd
os.system(cmd)
'''
print '\nBlocklist DE\n'
### Blocklist DE ###

### Feodo ###
print '\nFeodo\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Feodo/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Feodo.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_Feodo.py'
print cmd
os.system(cmd)
'''
print '\nFeodo\n'
### Feodo ###

''' Palevo Has been DISCONTINUED :/
### Palevo ###
print '\nPalevo\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Palevo/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Palevo.py'
print cmd
os.system(cmd)

# cmd = 'ipython Pull_Palevo.py'
# print cmd
# os.system(cmd)

print '\nPalevo\n'
### Palevo ###
'''


### RansomWare ###
print '\nRansomWare\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/RansomeWare/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_RansomWare.ipy'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_RansomWare.py'
print cmd
os.system(cmd)
'''
print '\nRansomWare\n'
### RansomWare ###

### Spamhaus ###
print '\nSpamhaus\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Spamhaus/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Spamhaus.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_Spamhaus.py'
print cmd
os.system(cmd)
'''
print '\nSpamhaus\n'
### Spamhaus ###

### Talos Intel ###
print '\nTalos\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/TalosIntel/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Talos_Intel.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_Talos_Intel.py'
print cmd
os.system(cmd)
'''
print '\nTalos\n'
### Talos Intel ###

### Zeus ###
print '\nZeus\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Zeus/'
print 'cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Zeus.py'
print cmd
os.system(cmd)
'''
cmd = 'ipython Pull_Zeus.py'
print cmd
os.system(cmd)
'''
print '\nZeus\n'
### Zeus ###

### VERIS Community DB ###

### VERIS Community DB ###

### Anti Hacker Alliance ###
print '\nAnti Hacker Alliance\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Anti_Hacker_Aliance/'
print 'cd '+ cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Anti_Hacker_Alliance.py'
print cmd
os.system(cmd)
print '\nAnti Hacker Alliance\n'
### Anti Hacker Alliance ###

### Long Tail ###
print '\nLong Tail\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/LongTail/'
print 'cd '+ cmd
os.chdir(cmd)

cmd = 'ipython Scrape_FULL_LongTail.py'
print cmd
os.system(cmd)
print '\nLong Tail\n'
### Long Tail ###

### Bambenek Consulting ###
print '\nBambenek Consulting\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Bambenek_Consulting/'
print cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Bambenek_Consulting.py'
print cmd
os.system(cmd)
print '\nBambenek Consulting\n'
### Bambenek Consulting ###

### DShield ###
print '\nDShield\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/DShield/'
print cmd
os.chdir(cmd)

cmd = 'ipython Scrape_DShield.py'
print cmd
os.system(cmd)
print '\nDShield\n'
### DShield ###

