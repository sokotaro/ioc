import os, subprocess, datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

# Source Directories
alienDir      = '/home/katayama/Desktop/home/IOC_Databases/AlienVaultOTX_Python/OTX-Python-SDK/'
criticalDir   = '/home/katayama/Desktop/home/IOC_Databases/Critical_Stack/'
charlesDir    = '/home/katayama/Desktop/home/IOC_Databases/Charles_Haley_SSH/'
BlocklistDir  = '/home/katayama/Desktop/home/IOC_Databases/Blocklist_DE/'
FeodoDir      = '/home/katayama/Desktop/home/IOC_Databases/Feodo/'
RansomWareDir = '/home/katayama/Desktop/home/IOC_Databases/RansomeWare/'
SpamhausDir   = '/home/katayama/Desktop/home/IOC_Databases/Spamhaus/'
TalosDir      = '/home/katayama/Desktop/home/IOC_Databases/TalosIntel/'
ZeusDir       = '/home/katayama/Desktop/home/IOC_Databases/Zeus/'
AntiHackerDir = '/home/katayama/Desktop/home/IOC_Databases/Anti_Hacker_Aliance/'
LongTailDir   = '/home/katayama/Desktop/home/IOC_Databases/LongTail/'
BambenekDir   = '/home/katayama/Desktop/home/IOC_Databases/Bambenek_Consulting/'
DShieldDir    = '/home/katayama/Desktop/home/IOC_Databases/DShield/'

# Source Scripts
alienPy       = 'ipython Scrape_Alien_Vault_Python.py'
criticalPy    = 'ipython ScrapeCriticalStack.py'
charlesPy     = 'ipython Scrapy_Charley_Haley_SSH.py'
BlocklistPy   = 'ipython Scrape_Blocklist_DE.py'
FeodoPy       = 'ipython Scrape_Feodo.py'
RansomeWarePy = 'ipython Scrape_RansomWare.ipy'
SpamhausPy    = 'ipython Scrape_Spamhaus.py'
TalosPy       = 'ipython Scrape_Talos_Intel.py'
ZeusPy        = 'ipython Scrape_Zeus.py'
AntiHackerPy  = 'ipython Scrape_Anti_Hacker_Alliance.py'
LongTailPy    = 'ipython Scrape_FULL_LongTail.py'
BambenekPy    = 'ipython Scrape_Bambenek_Consulting.py'
DShieldPy     = 'ipython Scrape_DShield.py'

### Alien Vault OTX ###
###################################################################################################
#                                                                                                 #
#   Prior to running this, execute AlienVaultOTX_subscribe.py to subscribe to all latest feeds    #
#                                                                                                 #
###################################################################################################
logging.info('\nAlien Vault OTX\n')
os.chdir(alienDir)
p = subprocess.Popen(alienPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nAlien Vault OTX\n')
### Alien Vault OTX ###

### Critical Stack ###
logging.info('\nCritical Stack\n')
os.chdir(criticalDir)
p = subprocess.Popen('critical-stack-intel list', shell=True)
p.wait()
logging.info((p.returncode))

p = subprocess.Popen('critical-stack-intel pull', shell=True)
p.wait()
logging.info((p.returncode))

p = subprocess.Popen("cp -f /opt/critical-stack/frameworks/intel/master-public.bro.dat master-public.bro.dat", shell=True)
p.wait()
logging.info((p.returncode))

p = subprocess.Popen(criticalPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nCritical Stack\n')
### Critical Stack ###

### Charles Haley SSH ###
logging.info('\nCharles Haley\n')
os.chdir(charlesDir)
p = subprocess.Popen(charlesPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nCharles Haley\n')
### Charles Haley SSH ###

### Blocklist DE ###
logging.info('\nBlocklist DE\n')
os.chdir(BlocklistDir)
p = subprocess.Popen(BlocklistPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nBlocklist DE\n')
### Blocklist DE ###

### Feodo ###
logging.info('\nFeodo\n')
os.chdir(FeodoDir)
p = subprocess.Popen(FeodoPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nFeodo\n')
### Feodo ###

''' Palevo Has been DISCONTINUED :/
### Palevo ###
logging.info('\nPalevo\n'
cmd = '/home/katayama/Desktop/home/IOC_Databases/Palevo/'
logging.info('cd ' + cmd
os.chdir(cmd)

cmd = 'ipython Scrape_Palevo.py'
logging.info(cmd
os.system(cmd)

# cmd = 'ipython Pull_Palevo.py'
# logging.info(cmd
# os.system(cmd)

logging.info('\nPalevo\n'
### Palevo ###
'''


### RansomWare ###
logging.info('\nRansomWare\n')
os.chdir(RansomWareDir)
p = subprocess.Popen(RansomeWarePy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nRansomWare\n')
### RansomWare ###

### Spamhaus ###
logging.info('\nSpamhaus\n')
os.chdir(SpamhausDir)
p = subprocess.Popen(SpamhausPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nSpamhaus\n')
### Spamhaus ###

### Talos Intel ###
logging.info('\nTalos\n')
os.chdir(TalosDir)
p = subprocess.Popen(TalosPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nTalos\n')
### Talos Intel ###

### Zeus ###
logging.info('\nZeus\n')
os.chdir(ZeusDir)
p = subprocess.Popen(ZeusPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nZeus\n')
### Zeus ###

### VERIS Community DB ###

### VERIS Community DB ###

### Anti Hacker Alliance ###
logging.info('\nAnti Hacker Alliance\n')
os.chdir(AntiHackerDir)
p = subprocess.Popen(AntiHackerPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nAnti Hacker Alliance\n')
### Anti Hacker Alliance ###

### Long Tail ###
logging.info('\nLong Tail\n')
os.chdir(LongTailDir)
p = subprocess.Popen(LongTailPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nLong Tail\n')
### Long Tail ###

### Bambenek Consulting ###
logging.info('\nBambenek Consulting\n')
os.chdir(BambenekDir)
p = subprocess.Popen(BambenekPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nBambenek Consulting\n')
### Bambenek Consulting ###

### DShield ###
logging.info('\nDShield\n')
os.chdir(DShieldDir)
p = subprocess.Popen(DShieldPy, shell=True)
p.wait()
logging.info((p.returncode))
logging.info('\nDShield\n')
### DShield ###

