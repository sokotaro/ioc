import os
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

cmd = 'curl https://api.dnsdb.info > tmp.txt'
print cmd
os.system(cmd)

with open('tmp.txt') as f:
    html = f.readlines()

html = "".join(html)
data = strip_tags(html)

with open('out.txt', 'w') as f:
    f.writelines(data)

cmd = 'lynx https://api.dnsdb.info'
print cmd
os.system(cmd)