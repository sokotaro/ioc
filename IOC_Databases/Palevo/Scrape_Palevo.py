###########################################################
##                                                       ##
##     https://palevotracker.abuse.ch/blocklists.php     ##
##                                                       ##
##  IP addresses which are being used as botnet C&C for  ##
##  the Palevo crimeware                                 ##
##                                                       ##
###########################################################

import sys
from datetime import datetime, timedelta

import locale
import MySQLdb as mdb
import time
import os

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    
    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Palevo'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time
    
    cmd = "RENAME TABLE Palevo TO Palevo" + create_time
    cursor.execute(cmd)
        
    # cursor.execute("DROP TABLE IF EXISTS Talos_Intel")
    
    cursor.execute("CREATE TABLE Palevo(ip VARCHAR(16) NOT NULL)")    
    # '''
    cmd = "mv palevo.txt palevo"+create_time+".txt"
    print cmd
    os.system(cmd)
    
    cmd = 'wget https://palevotracker.abuse.ch/blocklists.php?download=ipblocklist -O palevo.txt'
    print cmd
    os.system(cmd)
    filename = 'palevo.txt'
    
    num_lines = sum(1 for line in open(filename))
    print 'num_lines = ', "{:,}".format(num_lines)

    with open(filename) as f:
        print 'Reading File: ', filename
        blob = f.read().splitlines()
    blob.pop(0)
    
    count = 0
    for ip in blob:
        ip = ip.strip()
        
        cursor.execute("INSERT INTO Palevo(ip) VALUES(%s)",[ip])

        count += 1            
        if(count % (num_lines/10) == 0):
            print 'Processed: ', 
            amt = ((10*count)/(num_lines/10))
            amt = str(amt)+'% | '
            print amt,
            print "{:,}".format(count),
            print 'lines'

