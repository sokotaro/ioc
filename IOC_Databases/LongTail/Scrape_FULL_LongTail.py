import sys
import locale
import MySQLdb as mdb
import time
import os
from HTMLParser import HTMLParser
from datetime import datetime, timedelta
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()
    
logging.info('Connecting to openioc database...')

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()

    try:
        cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                       WHERE table_schema = 'openioc' \
                       AND table_name = 'Long_Tail'")
    except Exception as e:
        logging.debug(e)

    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    logging.info(create_time)
    
    cmd = "RENAME TABLE Long_Tail TO Long_Tail_" + yesterday
    try:
        cursor.execute(cmd)
    except Exception as e:
        logging.debug(e)
        pass
    
    # cursor.execute("DROP TABLE IF EXISTS Long_Tail")
    try:
        cursor.execute("CREATE TABLE Long_Tail(name VARCHAR(255), ip VARCHAR(16) NOT NULL,\
                       victim VARCHAR(255), date VARCHAR(255))")    
    except Exception as e:
        logging.debug(e)

    cmd = 'curl http://longtail.it.marist.edu/honey/ip_attacks.shtml > tmp.txt'
    logging.info((cmd))
    os.system(cmd)
    
    with open('tmp.txt') as f:
        html = f.readlines()

    output  = []
    output2 = []

    for line in html:
        if 'ls' not in line:
            if '<a name=' in line:
                line = line.strip('<a name="').strip('"></a>\n').split()
                name   = line[0]
                ip     = '.'.join(line[1].split('.')[:4])
                victim = '.'.join(line[1].split('.')[4:]).split('-')[0]
                date   = '.'.join(line[1].split('.')[4:]).split('-')[1]
                output.append(str(name)+'\t'+str(ip)+'\t'+str(victim)+'\t'+str(date)+'\n')
                output2.append(str(ip)+'\n')

            if 'From' in line:
                name   = line.strip('<a name="').strip('\n').split()[3].replace('\">', '').split('dict-')[1].strip('.txt')
                ip     = '.'.join(line.strip('\n').replace(' ','').replace('-->','.').split('From:')[1].split('.')[:4])
                victim = line.strip('\n').split('To: ')[1].split()[0]
                date   = line.strip('\n').split('on ')[1]
                
                output.append(str(name)+'\t'+str(ip)+'\t'+str(victim)+'\t'+str(date)+'\n')
                output2.append(str(ip)+'\n')

    with open('output.txt', 'w') as f:
        f.writelines(output)

    with open('output2.txt', 'w') as f:
        f.writelines(output2)
        
    filename = 'output.txt'
    
    num_lines = sum(1 for line in open(filename))
    logging.info('num_lines = ' + str("{:,}".format(num_lines)))
    
    
    with open(filename) as f:
        logging.info('Reading File: ' + str(filename))
        blob = f.readlines()
    
    count = 0
    for line in blob:
        if '#' not in line:
            data = line.strip()
            data = data.split('\t')
            name   = data[0]
            ip     = data[1]
            victim = data[2]
            date   = data[3]

            cursor.execute("INSERT INTO Long_Tail(name, ip, victim, date) VALUES(%s,%s,%s,%s)",(name, ip, victim, date))

            count += 1            
            try:
                if(count % (num_lines/10) == 0):
                    amt = ((10*count)/(num_lines/10))
                    amt = str(amt)+'% | '
                    logging.info('Processed: ' + str(amt) + str("{:,}".format(count)) + 'lines')
            except Exception as e:
                logging.debug(e)
