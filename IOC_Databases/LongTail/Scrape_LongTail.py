import sys
'''
sys.path.insert(0, '/usr/lib/python2.7/dist-packages/')
'''
from datetime import datetime, timedelta

import locale
import MySQLdb as mdb
import time
import os

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()

    cursor.execute("SELECT create_time FROM INFORMATION_SCHEMA.TABLES \
                    WHERE table_schema = 'openioc' \
                    AND table_name = 'Long_Tail'")
    create_time = cursor.fetchall()
    create_time = '_'+str(create_time[0][0]).split()[0].replace('-','_')
    print create_time
    
    cmd = "RENAME TABLE Long_Tail TO Long_Tail" + create_time
    cursor.execute(cmd)
    
    
    # cursor.execute("DROP TABLE IF EXISTS Long_Tail")
    cursor.execute("CREATE TABLE Long_Tail(ip VARCHAR(16) NOT NULL,\
                    hit_count INT(4) NOT NULL DEFAULT '0')")    

    cmd = 'mv historical-ip-addresses.txt historical-ip-addresses'+create_time+'.txt'
    print cmd
    os.system(cmd)
                    
    cmd = 'wget http://longtail.it.marist.edu/honey/historical-ip-addresses.txt'
    print cmd
    os.system(cmd)
    filename = 'historical-ip-addresses.txt'
    
    num_lines = sum(1 for line in open(filename))
    print 'num_lines = ', "{:,}".format(num_lines)
    
    
    with open(filename) as f:
        print 'Reading File: ', filename
        blob = f.readlines()
    
    count = 0
    for line in blob:
        if '#' not in line:
            data = line.strip()
            data = data.split(' ')
            ip = data[1]
            hit_count = data[0]

            cursor.execute("INSERT INTO Long_Tail(ip, hit_count) VALUES(%s,%s)",(ip, hit_count))

            count += 1            
            if(count % (num_lines/10) == 0):
                print 'Processed: ', 
                amt = ((10*count)/(num_lines/10))
                amt = str(amt)+'% | '
                print amt,
                print "{:,}".format(count),
                print 'lines'
