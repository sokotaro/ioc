###########################################################
##                                                       ##
##          https://feodotracker.abuse.ch/               ##
##                                                       ##
##          Feodo IP Blocklist                           ##
##                                                       ##
###########################################################

# Import required modules
import os
import MySQLdb as mdb
import requests
from bs4 import BeautifulSoup
import datetime

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))

url   = 'https://feodotracker.abuse.ch/'
cmd   = 'wget ' + url + ' -O feodo_' + yesterday
print cmd
os.system(cmd)

data = []
with open('feodo_'+yesterday) as f:
    data = f.read()
soup  = BeautifulSoup(data, 'lxml')
table = soup.find(class_='sortable')

output       = []

for row in table.find_all('tr')[1:]:
    col = row.find_all('td')
    col_1 = col[0].string
    col_2 = col[1].string
    col_3 = col[2].string
    col_4 = col[3].string
    col_5 = col[4].string
    col_6 = col[5].string
    if len(str(col[6])) > 9:
        col_7 = str(col[6]).split(' title=')[1].split(' width')[0].strip('\"')
    else:
        col_7 = col[6].string
    col_8 = col[7].string

    if col_1 is None:
        col_1 = ''
    if col_2 is None:
        col_2 =''
    if col_3 is None:
        col_3 =''
    if col_4 is None:
        col_4 =''
    if col_5 is None:
        col_5 =''
    if col_6 is None:
        col_6 =''
    if col_7 is None:
        col_7 =''
    if col_8 is None:
        col_8 =''

    output.append(str(col_1.encode('utf-8')) + '\t' + str(col_2.encode('utf-8')) + '\t' + str(col_3.encode('utf-8')) + '\t' + str(col_4.encode('utf-8')) + '\t' + str(col_5.encode('utf-8')) + '\t' + str(col_6.encode('utf-8')) + '\t' + str(col_7.encode('utf-8')) + '\t' + str(col_8.encode('utf-8')) + '\n')

#  MySQL does not like '-'
yesterday = yesterday.replace('-','_')

print 'Connecting to openioc database...'

con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    query  = "CREATE TABLE Foedo__" + yesterday + "(first_seen VARCHAR(255), version VARCHAR(255), ip VARCHAR(16) NOT NULL, status VARCHAR(255), SBL VARCHAR(255), ASN VARCHAR(255), country VARCHAR(16), last_seen VARCHAR(255))"
    cursor.execute(query)
    total  = len(output)
    count  = 0

    for line in output:
        line = line.strip()
        line = line.split('\t')

        first_seen = line[0]
        version    = line[1]
        ip         = line[2]
        status     = line[3]
        SBL        = line[4]
        ASN        = line[5]
        country    = line[6]
        last_seen  = line[7]

        query = "INSERT INTO Foedo__" + yesterday + "(first_seen, version, ip, status, SBL, ASN, country, last_seen) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
        cursor.execute(query,(first_seen, version, ip, status, SBL, ASN, country, last_seen))
    
        count += 1
    
with open('Feodo_' + yesterday + '.txt', 'w') as f:
    f.writelines(output)
