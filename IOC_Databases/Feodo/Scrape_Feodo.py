###########################################################
##                                                       ##
##          https://feodotracker.abuse.ch/               ##
##                                                       ##
##          Feodo IP Blocklist                           ##
##                                                       ##
###########################################################

# Import required modules
import os
import MySQLdb as mdb
import requests
from bs4 import BeautifulSoup
import datetime
import logging

yesterday = str(datetime.date.today() - datetime.timedelta(days = 1))

data = []
with open('monitor/monitor_domain_'+yesterday) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'domain' + '\t' + 'Feodo C&C Trojan vB' + '\n')

with open('monitor/monitor_badips_'+yesterday) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'ipv4' + '\t' + 'Feodo C&C Trojan vB' + '\n')

with open('monitor/monitor_blocklist_'+yesterday) as f:
    for line in f:
        line = line.strip()
        if ('#' not in line) and (line != '') and ('ERROR' not in line):
            data.append(line + '\t' + 'ipv4' + '\t' + 'Feodo C&C Trojan vA vC vD' + '\n')


#  MySQL does not like '-'
yesterday = yesterday.replace('-','_')

logging.basicConfig(filename='/home/katayama/Desktop/status_'+yesterday+'.log',level=logging.DEBUG)

logging.info('Connecting to openioc database...')
logging.info('processing data...')
con = mdb.connect('localhost', 'root', '#teddy#', 'openioc')

with con:
    cursor = con.cursor()
    query  = "CREATE TABLE Feodo_" + yesterday + "(ioc VARCHAR(255), ioc_type VARCHAR(255), attack VARCHAR(255))"
    try:
        cursor.execute(query)
    except Exception as e:
        logging.debug(e)

    total  = len(data)
    count  = 0

    for line in data:
        line = line.strip()
        line = line.split('\t')

        ioc       = line[0]
        ioc_type  = line[1]
        attack    = line[2]

        query = "INSERT INTO Feodo_" + yesterday + "(ioc, ioc_type, attack) VALUES(%s,%s,%s)"
        cursor.execute(query,(ioc, ioc_type, attack))
    
        count += 1
logging.info('save data...')
with open('data/Feodo_' + yesterday + '.txt', 'w') as f:
    f.writelines(data)
