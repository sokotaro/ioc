#!/usr/bin/python
# -*- coding: utf-8 -*-
#from OTXv2 import OTXv2
#from pandas.io.json import json_normalize
#from datetime import datetime, timedelta
import MySQLdb as mdb
import sys

con = mdb.connect('localhost', 'iocuser', '#teddy#', 'openioc')

file = 'Master_File'

fileData = []

with con:
    cur = con.cursor()
    #-----------------------------------------------------#
    #--------------------TOR NETWORK-------------------#
    #-----------------------------------------------------#
    cur.execute("DROP TABLE IF EXISTS Tor_Exit_Nodes")
    cur.execute("CREATE TABLE Tor_Exit_Nodes(Id INT PRIMARY KEY AUTO_INCREMENT, \
                 exit_node VARCHAR(128), published_date VARCHAR(32), published_time VARCHAR(32), last_status_date VARCHAR(128), last_status_time VARCHAR(32), exit_address VARCHAR(128), exit_address_date VARCHAR(32), exit_address_time VARCHAR(32))")

    f = open(file)
    i = 0

    print('Uploading TOR Data')
    for line in f:
		if ('ExitNode' in line):
			ExitNode = line.split(' ')
			ExitNode.pop(0)
			ExitNode = ' '.join(ExitNode)
                        
#			print('ExitNode =  ' + line),
		elif ('Published' in line):
			Published = line.split(' ')
			Published.pop(0)
#			Published = ' '.join(Published)
#			print('Published = ' + line),
		elif ('LastStatus' in line):
			LastStatus = line.split(' ')
			LastStatus.pop(0)
#			LastStatus = ' '.join(LastStatus)
#			print('LastStatus = ' + line),
		elif ('ExitAddress' in line):
			ExitAddress = line.split(' ')
			ExitAddress.pop(0)
#			ExitAddress = ' '.join(ExitAddress)
			cur.execute("INSERT INTO Tor_Exit_Nodes(exit_node, published_date, published_time, last_status_date, last_status_time, exit_address, exit_address_date, exit_address_time) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)",(ExitNode,Published[0],Published[1],LastStatus[0],LastStatus[1],ExitAddress[0],ExitAddress[1],ExitAddress[2],))
#			print('ExitAddress = ' + line),
		else:
			print('*'),
#        fileData.append(data)
#		i += 1
#		if (i % 100) == 0:
#			print('*'),
#			break

#    cur.execute("SELECT * FROM Tor_Exit_Nodes")

#    rows = cur.fetchall()

#    for row in rows:
#        print row
    #-----------------------------------------------------#
    #--------------------TOR NETWORK-------------------#
    #-----------------------------------------------------#

